/*****************************************************************************
 *
 * Copyright (C) 2022 Bjarne von Horn (vh at igh dot de)
 *
 * This file is part of the QtPdCom library.
 *
 * The QtPdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The QtPdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdCom library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

#include "QtPdComTests.h"

#include "../server.h"

#include <QSslCertificate>
#include <QSslKey>
#include <QTest>
#include <QtPdCom1/LoginManager.h>
#include <QtPdCom1/Process.h>
#include <QtPdCom1/ScalarSubscriber.h>
#include <QtPdCom1/ScalarVariable.h>
#include <conf.h>
#include <pdcom5/SizeTypeInfo.h>
#include <tuple>


class MySubscriber : public QtPdCom::ScalarSubscriber
{
    void newValues(std::chrono::nanoseconds ts) override
    {
        double d;
        PdCom::details::copyData(
                &d, PdCom::TypeInfo::double_T, getData(),
                getVariable().getTypeInfo().type, 1);
        values_.emplace_back(ts, d);
    }

  public:
    std::vector<std::pair<std::chrono::nanoseconds, double>> values_;
};

void QtPdCom1Tests::test_broadcast()
{
    struct Broadcast
    {
        QString text, attribute;
    };

    TestServer server;
    server.set_config_file_path(std::string(config_dir) + "/pdserv.conf");
    server.prepare();
    server.start();
    QList<Broadcast> received_broadcasts;

    QtPdCom::Process process, p2;

    connect(&process, &QtPdCom::Process::broadcastReceived, this,
            [&received_broadcasts](
                    const QString &message, const QString &attr,
                    std::uint64_t time_ns, const QString &user) {
                received_broadcasts.append({message, attr});
            });

    process.connectToHost("localhost");
    p2.connectToHost("localhost");

    QTRY_VERIFY(process.isConnected());
    QTRY_VERIFY(p2.isConnected());

    const QString txt1  = "Text";
    const QString txt2  = "letter";
    const QString attr2 = "action";

    p2.sendBroadcast(txt1);
    QTRY_COMPARE(received_broadcasts.size(), 1);
    QCOMPARE(received_broadcasts.front().attribute, QString("text"));
    QCOMPARE(received_broadcasts.front().text, txt1);
    received_broadcasts.clear();

    p2.sendBroadcast(txt2, attr2);
    QTRY_COMPARE(received_broadcasts.size(), 1);
    QCOMPARE(received_broadcasts.front().attribute, attr2);
    QCOMPARE(received_broadcasts.front().text, txt2);
}

void QtPdCom1Tests::test_cancel_futures()
{
    TestServer server;
    server.prepare();
    server.start();

    std::unique_ptr<QtPdCom::Process> process {new QtPdCom::Process()};
    process->connectToHost("localhost");

    QTRY_VERIFY(process->isConnected());

    const auto f1 = process->find("/myvar");
    const auto f2 = process->list();
    const auto f3 = process->pingQt();
    const auto f4 = process->getClientStatisticsQt();

    process.reset();

    QVERIFY(f1.isCanceled());
    QVERIFY(f2.isCanceled());
    QVERIFY(f3.isCanceled());
    QVERIFY(f4.isCanceled());
}

void QtPdCom1Tests::test_client_statistics()
{
    TestServer server;
    server.prepare();
    server.start();

    const QString appname = "TestApp";

    QtPdCom::Process process;
    process.setApplicationName(appname);
    process.connectToHost("localhost");

    QTRY_VERIFY(process.isConnected());

    std::vector<PdCom::ClientStatistics> reply;

    process.getClientStatistics(
            this, [&reply](std::vector<PdCom::ClientStatistics> ans) {
                reply = std::move(ans);
            });

    QTRY_COMPARE(reply.size(), 1);
    QCOMPARE(reply.front().application_name_, appname.toStdString());
    QCOMPARE(reply.front().received_bytes_, process.getTxBytes());
    // reply is not accounted from server
    QVERIFY(process.getRxBytes() > reply.front().sent_bytes_);
    QVERIFY(!reply.front().name_.empty());
}

void QtPdCom1Tests::test_ping()
{
    TestServer server;
    server.prepare();
    server.start();

    QtPdCom::Process process;
    process.connectToHost("localhost");

    QTRY_VERIFY(process.isConnected());

    process.ping(this, &QtPdCom1Tests::onPingReply);
    QTRY_COMPARE(ping_replies, 1);

    int captured_pings = 0;
    process.ping(
            this, [&captured_pings](QtPdCom1Tests &) { ++captured_pings; });
    QTRY_COMPARE(captured_pings, 1);
    captured_pings = 0;
    process.ping(this, [&captured_pings] { ++captured_pings; });
    QTRY_COMPARE(captured_pings, 1);
}

void QtPdCom1Tests::test_ssl_custom_ca()
{
    TestServer server;
    server.set_config_file_path(
            std::string(config_dir) + "/pdserv.tls-simple.conf");
    server.prepare();
    server.start();

    QtPdCom::Process process;

    const auto cas = [] {
        QFile f(QString(cert_dir) + "/ca.pem");
        f.open(QIODevice::ReadOnly);
        return QSslCertificate::fromDevice(&f);
    }();
    QVERIFY2(!cas.empty(), "Loading CA failed");

    process.setCustomCAs(cas);
    process.setCaMode(QtPdCom::Process::SslCaMode::CustomCAs);
    process.connectToHost("localhost", 4523);
    QTRY_VERIFY(process.isConnected());
    QVERIFY(process.name() == "TestServer");
}

void QtPdCom1Tests::test_ssl_ignore_ca()
{
    TestServer server;
    server.set_config_file_path(
            std::string(config_dir) + "/pdserv.tls-simple.conf");
    server.prepare();
    server.start();

    QtPdCom::Process process;

    process.setCaMode(QtPdCom::Process::SslCaMode::IgnoreCertificate);
    process.connectToHost("localhost", 4523);
    QTRY_VERIFY(process.isConnected());
    QVERIFY(process.name() == "TestServer");
}

static std::tuple<QSslCertificate, QSslKey> loadKeyCert(QString what)
{
    QFile key(QString(cert_dir) + "/" + what + ".key");
    QFile cert(QString(cert_dir) + "/" + what + ".pem");
    cert.open(QIODevice::ReadOnly);
    key.open(QIODevice::ReadOnly);
    return std::make_tuple(QSslCertificate {&cert}, QSslKey {&key, QSsl::Rsa});
}

void QtPdCom1Tests::test_ssl_client_cert_success()
{
    TestServer server;
    server.set_config_file_path(
            std::string(config_dir) + "/pdserv.tls-clientauth.conf");
    server.prepare();
    server.start();

    QtPdCom::Process process;

    const auto cas = [] {
        QFile f(QString(cert_dir) + "/ca.pem");
        f.open(QIODevice::ReadOnly);
        return QSslCertificate::fromDevice(&f);
    }();
    QVERIFY2(!cas.empty(), "Loading CA failed");

    const auto priv = loadKeyCert("client1");
    QVERIFY(!std::get<0>(priv).isNull() && !std::get<1>(priv).isNull());

    process.setCustomCAs(cas);
    process.setClientCertificate(std::get<0>(priv), std::get<1>(priv));
    process.setCaMode(QtPdCom::Process::SslCaMode::CustomCAs);
    process.connectToHost("localhost", 4523);
    QTRY_VERIFY(process.isConnected());
    QVERIFY(process.name() == "TestServer");
}

void QtPdCom1Tests::test_ssl_wrong_client_cert()
{
    TestServer server;
    server.set_config_file_path(
            std::string(config_dir) + "/pdserv.tls-clientauth.conf");
    server.prepare();
    server.start();

    QtPdCom::Process process;

    const auto cas = [] {
        QFile f(QString(cert_dir) + "/ca.pem");
        f.open(QIODevice::ReadOnly);
        return QSslCertificate::fromDevice(&f);
    }();
    QVERIFY2(!cas.empty(), "Loading CA failed");

    const auto priv = loadKeyCert("noca_client");
    QVERIFY(!std::get<0>(priv).isNull() && !std::get<1>(priv).isNull());

    process.setCustomCAs(cas);
    process.setClientCertificate(std::get<0>(priv), std::get<1>(priv));
    process.setCaMode(QtPdCom::Process::SslCaMode::CustomCAs);
    process.connectToHost("localhost", 4523);
    QTRY_VERIFY(process.getConnectionState() == QtPdCom::Process::ConnectError);
    QVERIFY(!process.isConnected());
}

void QtPdCom1Tests::test_ssl_verify_dns()
{
    TestServer server;
    server.set_config_file_path(
            std::string(config_dir) + "/pdserv.tls-simple.conf");
    server.prepare();
    server.start();

    QtPdCom::Process process;

    const auto cas = [] {
        QFile f(QString(cert_dir) + "/ca.pem");
        f.open(QIODevice::ReadOnly);
        return QSslCertificate::fromDevice(&f);
    }();
    QVERIFY2(!cas.empty(), "Loading CA failed");
    process.setCustomCAs(cas);
    process.setCaMode(QtPdCom::Process::SslCaMode::CustomCAs);
    // this ip is not in DnsAltName
    process.connectToHost("127.0.0.11", 4523);
    QTRY_VERIFY(process.getConnectionState() == QtPdCom::Process::ConnectError);
    QVERIFY(!process.isConnected());
}

void QtPdCom1Tests::test_ssl_failed()
{
    TestServer server;
    server.set_config_file_path(
            std::string(config_dir) + "/pdserv.tls-simple.conf");
    server.prepare();
    server.start();

    QtPdCom::Process process;
    process.setCaMode(QtPdCom::Process::SslCaMode::DefaultCAs);
    process.connectToHost("localhost", 4523);
    QTRY_VERIFY(process.getConnectionState() == QtPdCom::Process::ConnectError);
    QVERIFY(!process.isConnected());
}

void QtPdCom1Tests::test_login_optional()
{
    using QtPdCom::LoginManager;
    TestServer server;
    server.set_config_file_path(
            std::string(config_dir) + "/pdserv.sasl_voluntary.conf");
    server.prepare();
    server.start();

    int success_count = 0, failed_count = 0, need_credentials = 0;

    QtPdCom::Process process;
    process.connectToHost("localhost", 2345);
    QTRY_VERIFY(process.isConnected());

    LoginManager lm;

    connect(&lm, &LoginManager::loginSuccessful, this,
            [&success_count] { ++success_count; });
    connect(&lm, &LoginManager::loginFailed, this,
            [&failed_count] { ++failed_count; });

    auto c =
            connect(&lm, &LoginManager::needCredentials, this,
                    [&need_credentials, &lm] {
                        ++need_credentials;
                        lm.setAuthName("bob@example.com");
                        lm.setPassword("Bob!");
                    });

    process.setLoginManager(&lm);

    QCOMPARE(success_count, 0);
    QCOMPARE(failed_count, 0);
    lm.login();
    QTRY_COMPARE(success_count, 1);
    QCOMPARE(failed_count, 0);
    lm.logout();
    // login again and verify that lm asks again for credentials
    QObject::disconnect(c);
    connect(&lm, &LoginManager::needCredentials, this,
            [&need_credentials] { ++need_credentials; });
    lm.clearCredentials();
    need_credentials = 0;
    lm.login();
    QTRY_COMPARE(need_credentials, 1);
    QCOMPARE(success_count, 1);
    QCOMPARE(failed_count, 0);
}

void QtPdCom1Tests::test_login_success_synchronous()
{
    using QtPdCom::LoginManager;
    TestServer server;
    server.set_config_file_path(std::string(config_dir) + "/pdserv.sasl.conf");
    server.prepare();
    server.start();

    int success_count = 0, failed_count = 0, need_credentials = 0;

    LoginManager lm;
    connect(&lm, &LoginManager::loginSuccessful, this,
            [&success_count] { ++success_count; });
    connect(&lm, &LoginManager::loginFailed, this,
            [&failed_count] { ++failed_count; });

    connect(&lm, &LoginManager::needCredentials, this,
            [&need_credentials, &lm] {
                ++need_credentials;
                lm.setAuthName("bob@example.com");
                lm.setPassword("Bob!");
            });

    QtPdCom::Process process;
    process.setLoginManager(&lm);
    process.connectToHost("localhost", 2345);
    QTRY_VERIFY(process.isConnected());
    QCOMPARE(success_count, 1);
    QCOMPARE(failed_count, 0);
    QCOMPARE(need_credentials, 1);
}

void QtPdCom1Tests::test_login_success_queued()
{
    using QtPdCom::LoginManager;
    TestServer server;
    server.set_config_file_path(std::string(config_dir) + "/pdserv.sasl.conf");
    server.prepare();
    server.start();

    int success_count = 0, failed_count = 0, need_credentials = 0;

    LoginManager lm;
    connect(&lm, &LoginManager::loginSuccessful, this,
            [&success_count] { ++success_count; });
    connect(&lm, &LoginManager::loginFailed, this,
            [&failed_count] { ++failed_count; });

    // use a queued connection to get control flow back
    // before calling setAuthName()
    connect(
            &lm, &LoginManager::needCredentials, this,
            [&need_credentials, &lm] { ++need_credentials; },
            Qt::QueuedConnection);

    QtPdCom::Process process;
    process.setLoginManager(&lm);
    process.connectToHost("localhost", 2345);
    QTRY_COMPARE(need_credentials, 1);
    lm.setAuthName("bob@example.com");
    lm.setPassword("Bob!");
    lm.login();
    QTRY_VERIFY(process.isConnected());
    QCOMPARE(success_count, 1);
    QCOMPARE(failed_count, 0);
    QCOMPARE(need_credentials, 1);
}

void QtPdCom1Tests::test_login_failed_synchronous()
{
    using QtPdCom::LoginManager;
    TestServer server;
    server.set_config_file_path(std::string(config_dir) + "/pdserv.sasl.conf");
    server.prepare();
    server.start();

    int success_count = 0, failed_count = 0, need_credentials = 0;

    LoginManager lm;
    connect(&lm, &LoginManager::loginSuccessful, this,
            [&success_count] { ++success_count; });
    connect(&lm, &LoginManager::loginFailed, this,
            [&failed_count] { ++failed_count; });

    connect(&lm, &LoginManager::needCredentials, this,
            [&need_credentials, &lm] {
                ++need_credentials;
                lm.setAuthName("bob@example.com");
                lm.setPassword("ThisIsWrong");
            });

    QtPdCom::Process process;
    process.setLoginManager(&lm);
    process.connectToHost("localhost", 2345);
    QTRY_COMPARE(failed_count, 1);
    QVERIFY(!process.isConnected());
    QCOMPARE(success_count, 0);
    QCOMPARE(need_credentials, 1);
}

void QtPdCom1Tests::test_find()
{
    TestServer server;
    server.prepare();
    server.start();

    QtPdCom::Process process;
    process.connectToHost("localhost");

    QTRY_VERIFY(process.isConnected());

    PdCom::Variable var1;
    process.find(QString("/dir1/signal1"), this, [&var1](PdCom::Variable var) {
        var1 = std::move(var);
    });

    QTRY_VERIFY(!var1.empty());
    QCOMPARE(var1.getPath(), "/dir1/signal1");

    PdCom::Variable var2 = var1;
    process.find(
            QString("/unknown/variable"), this,
            [&var2](QtPdCom1Tests & /* dummy */, PdCom::Variable var) {
                var2 = std::move(var);
            });

    QTRY_VERIFY(var2.empty());

    MySubscriber subscriber;

    subscriber.setVariable(var1, {}, TestServer::task1_period);
    QTRY_VERIFY(subscriber.values_.size() >= 4);
    QVERIFY(abs(subscriber.values_.back().second - server.getsignal1()) < 10);
    // signal1 is monotonic
    QVERIFY(subscriber.values_[2] != subscriber.values_[3]);
}

void QtPdCom1Tests::test_find_for_loop()
{
    struct CallbackResult
    {
        QString expected_path;
        PdCom::Variable actual_found;
    };
    std::vector<CallbackResult> v;

    TestServer server;
    server.prepare();
    server.start();

    QtPdCom::Process process;
    process.connectToHost("localhost");

    QTRY_VERIFY(process.isConnected());


    struct TestSet
    {
        const char *path;
        double value;
        int (TestServer::*getValue)() const;
    };

    const std::vector<TestSet> test_sets {
            {"/dir1/param01", 1.0, &TestServer::getparam1},
            {"/dir1/paramNotFound", 0.0, nullptr},
            {"/dir1/param04", 4.0, &TestServer::getparam4},
            {"/dir1/param05", 5.0, &TestServer::getparam5},
    };

    const auto findAndSet = [this, &process,
                             &v](const QString &path, double value) {
        process.find(
                path, this, [path, value, &v](const PdCom::Variable &pd_var) {
                    v.push_back({path, pd_var});
                    if (!pd_var.empty()) {
                        pd_var.setValue(value);
                    }
                });
    };

    // in the second and third run, all known parameters are cached
    for (int run = 1; run < 4; ++run) {
        v.clear();

        for (const auto &ts : test_sets) {
            findAndSet(ts.path, ts.value * run);
        }

        QTRY_COMPARE(v.size(), test_sets.size());
        for (const auto &result : v) {
            // find corresponding testset
            const auto it = std::find_if(
                    test_sets.begin(), test_sets.end(),
                    [&result](TestSet const &i) {
                        return QString(i.path) == result.expected_path;
                    });
            QVERIFY2(it != test_sets.end(), "Invalid path in result");

            if (it->getValue) {
                // variable was found
                QVERIFY2(
                        !result.actual_found.empty(),
                        qPrintable(QString("%1 has been found")
                                           .arg(result.expected_path)));
                QCOMPARE(
                        QString(result.actual_found.getPath().c_str()),
                        result.expected_path);
                // correct value has been set
                QCOMPARE((server.*it->getValue)(), it->value * run);
            }
            else {
                if (!result.actual_found.empty()) {
                    QFAIL(qPrintable(QString("%1 has not been found, actual %2")
                                             .arg(result.expected_path)
                                             .arg(result.actual_found.getPath()
                                                          .c_str())));
                };
            }
        }
    }
}


void QtPdCom1Tests::test_poll_timer()
{
    TestServer server;
    server.prepare();
    server.start();

    QtPdCom::Process process;
    process.connectToHost("localhost");

    QTRY_VERIFY(process.isConnected());

    MySubscriber sub;

    sub.setVariable(&process, "/dir1/signal1", {}, {QtPdCom::poll_mode, 1.0});
    QTRY_VERIFY(sub.values_.size() >= 4);
    auto it     = sub.values_.begin();
    auto old_ts = it->first;
    ++it;
    for (; it != sub.values_.end(); ++it) {
        const auto diff = it->first - old_ts;
        old_ts          = it->first;
        QVERIFY2(
                abs(1.0 * std::nano::den - diff.count()) < 0.2 * std::nano::den,
                qPrintable(QString("difference of timestamps is %2 µs")
                                   .arg(diff.count() / 1000)));
    }
}

void QtPdCom1Tests::initTestCase()
{
    QtPdCom::LoginManager::InitLibrary();
}

QTEST_GUILESS_MAIN(QtPdCom1Tests)
