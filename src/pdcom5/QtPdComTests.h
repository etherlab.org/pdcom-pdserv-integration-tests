/*****************************************************************************
 *
 * Copyright (C) 2022 Bjarne von Horn (vh at igh dot de)
 *
 * This file is part of the QtPdCom library.
 *
 * The QtPdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The QtPdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the QtPdCom library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

#pragma once

#include <QObject>

class QtPdCom1Tests : public QObject
{
    Q_OBJECT

  public:
  private slots:

    void initTestCase();
    void test_broadcast();
    void test_cancel_futures();
    void test_client_statistics();
    void test_ping();
    void test_ssl_custom_ca();
    void test_ssl_ignore_ca();
    void test_ssl_verify_dns();
    void test_ssl_failed();
    void test_ssl_client_cert_success();
    void test_ssl_wrong_client_cert();
    void test_login_success_synchronous();
    void test_login_optional();
    void test_login_success_queued();
    void test_login_failed_synchronous();
    void test_find();
    void test_find_for_loop();
    void test_poll_timer();

  private:
    int ping_replies = 0;

    void onPingReply() { ++ping_replies; }
};
