import numpy as np  # this goes first, otherwise pdcom5 may not load properly
from pdcom5 import _PdComWrapper as PdComWrapper
from socket import socket, AF_INET, SOCK_STREAM
from pditserver.current import TestServer as Server
from pditserver.current import Loglevel
import ssl
import sys
from time import monotonic
from datetime import timedelta
import unittest
from conf import cert_dir, config_dir

"""Tests for pybind11 wrapper"""

"""Tests for pybind11 wrapper"""


class MyProc(PdComWrapper.Process):
    def __init__(self):
        super().__init__()
        self._connected = False
        self._sock: socket = None
        self._recieved_broadcasts = []
        self._found_vars = []

    def read(self, buf):
        return self._sock.recv_into(buf)

    def write(self, buf):
        self._sock.sendall(buf)

    def connected(self):
        self._connected = True

    def broadcastReply(self, message: str, attribute: str, time_ns: int, user: str):
        self._recieved_broadcasts.append(
            {
                "message": message,
                "attribute": attribute,
                "time_ns": time_ns,
                "user": user,
            }
        )

    def findReply(self, var: PdComWrapper.Variable):
        self._found_vars.append(var)

    def readUntil(self, predicate, context=None, max_tries=10):
        if context is None:
            context = self
        self.callPendingCallbacks()
        if predicate(context):
            return
        for _ in range(max_tries):
            try:
                self.asyncData()
            except TimeoutError:
                pass
            if predicate(context):
                return
        raise TimeoutError()


class MySubscriber(PdComWrapper.Subscriber):
    def __init__(self, period: PdComWrapper.Transmission):
        super().__init__(period)
        self._values = []
        self._state_changes = []
        self._subscription: PdComWrapper.Subscription = None

    def newValues(self, time_ns: timedelta):
        self._values.append((time_ns, self._subscription.value))

    def stateChanged(self, sub: PdComWrapper.Subscription):
        if sub != self._subscription:
            raise RuntimeError("Mismatching subscription")
        self._state_changes.append(sub.state)


class MyMessageManager(PdComWrapper.MessageManagerBase):
    def __init__(self):
        super().__init__()
        self._process_message = []
        self._get_message_reply = []
        self._active_messages = []

    def processMessage(self, m: PdComWrapper.Message):
        self._process_message.append(m)

    def getMessageReply(self, m: PdComWrapper.Message):
        self._get_message_reply.append(m)

    def activeMessagesReply(self, ms: "list[PdComWrapper.Message]"):
        self._active_messages.append(ms)


def sub_is_active(s: MySubscriber):
    return (
        len(s._state_changes) > 0
        and s._state_changes[-1] == PdComWrapper.Subscription.State.Active
    )


class PdCom5PyBase:
    def _doSetUp(self, config_path, socket_factory):
        self._server = Server()
        self._server.set_config_file_path(config_path)
        self._server.prepare()
        self._server.start()
        self._client = MyProc()
        self._client._sock = socket_factory()
        self.assertIsNotNone(self._client._sock)
        self._client.readUntil(lambda c: c._connected)

    def _doTearDown(self):
        self._client._sock.close()
        self._client = None
        self._server.clear()
        self._server = None
        print("teardown has finished", file=sys.stderr)

    def test_server_version_name(self):
        self.assertEqual(self._client.name, "TestServer")
        self.assertEqual(self._client.version, "1.2.3")

    def _do_check_broadcast(
        self, message: str, attribute: str, user: str = "anonymous"
    ):
        self._client._recieved_broadcasts = []
        self._client.broadcast(message, attribute)
        self._client.readUntil(lambda c: len(c._recieved_broadcasts) == 1)
        self.assertEqual(len(self._client._recieved_broadcasts), 1)
        m = self._client._recieved_broadcasts[0]
        self.assertEqual(m["message"], message)
        self.assertEqual(m["attribute"], attribute)
        self.assertEqual(m["user"], user)
        self.assertLess(abs(m["time_ns"].total_seconds() - monotonic()), 2)

    def test_broadcasts(self):
        self._do_check_broadcast("My'Message\"With&Special<chars>", "text")
        self._do_check_broadcast("My'Message\"With&Special<chars>", "action")

        def r(s):
            s._do_check_broadcast("My\0Message", "text")

        self.assertRaises(PdComWrapper.InvalidArgument, r, self)

    def test_periodic_subscription(self):
        self._client.find("/dir1/signal1")
        self._client.readUntil(lambda c: len(c._found_vars) > 0)
        self.assertEqual(len(self._client._found_vars), 1)
        var = self._client._found_vars[-1]
        self.assertFalse(var.empty)
        self.assertEqual(tuple(var.shape), (1,))
        decimation = 10
        period: timedelta = Server.task1_period * decimation
        subscriber = MySubscriber(PdComWrapper.Transmission(period))
        sub1 = PdComWrapper.Subscription(subscriber, var)
        subscriber._subscription = sub1

        self._client.readUntil(sub_is_active, context=subscriber)

        def read_5_values(s: MySubscriber):
            return len(s._values) >= 5

        self._client.readUntil(read_5_values, context=subscriber)
        sub1 = subscriber._subscription = None

        for i, j in zip(subscriber._values[:-1], subscriber._values[1:]):
            self.assertAlmostEqual(
                (i[0] + period).total_seconds(),
                j[0].total_seconds(),
                delta=Server.task1_period.total_seconds(),
            )
            self.assertEqual(i[1] + decimation, j[1])

    def test_process_message(self):
        mm = MyMessageManager()
        self._client.setMessageManager(mm)
        self._server.set_event(0, Loglevel.warn)
        self._client.readUntil(lambda mm: len(mm._process_message) == 1, mm)
        m: PdComWrapper.Message = mm._process_message[0]
        self.assertEqual(m.level, PdComWrapper.LogLevel.Warn)
        self.assertEqual(m.text, "First Index")
        self.assertEqual(m.path, "/Event1")

    def test_vector_variable(self):
        self._multidim_test(
            "/dir1/param02",
            (4,),
            np.asarray((2, 3, 4, 5)),
            lambda x: x.param2,
            (1,),
            (2,),
        )

    def test_matrix_variable(self):
        self._multidim_test(
            "/dir1/param03",
            (2, 3),
            np.asarray(((2, 3, 4), (5, 6, 7))),
            lambda x: x.param3.value,
            (0, 1),
            (1, 2),
        )

    def _multidim_test(self, path, shape, next_value, param, read_index, write_index):
        self._client.find(path)
        self._client.readUntil(lambda c: len(c._found_vars) > 0)
        self.assertEqual(len(self._client._found_vars), 1)
        var = self._client._found_vars[-1]
        self.assertFalse(var.empty)
        self.assertEqual(tuple(var.shape), shape)

        subscriber1 = MySubscriber(PdComWrapper.Transmission.event_mode)
        sub1 = PdComWrapper.Subscription(subscriber1, var)
        subscriber1._subscription = sub1
        self._client.readUntil(sub_is_active, context=subscriber1)

        selector1 = PdComWrapper.ScalarSelector(read_index)
        subscriber2 = MySubscriber(PdComWrapper.Transmission.event_mode)
        sub2 = PdComWrapper.Subscription(subscriber2, var, selector1)
        subscriber2._subscription = sub2
        self._client.readUntil(sub_is_active, context=subscriber2)

        var.setValue(next_value)
        self._client.readUntil(lambda s: len(s._values) > 0, context=subscriber1)
        self._client.readUntil(lambda s: len(s._values) > 0, context=subscriber2)

        self.assertTrue((subscriber1._values[-1][1] == next_value).all())
        expected1 = np.asarray(param(self._server))
        self.assertTrue((expected1 == next_value).all())
        self.assertEqual(
            subscriber2._values[-1][1].shape, (1,), "ScalarSelector gives scalar"
        )
        self.assertEqual(subscriber2._values[-1][1][0], next_value[read_index])

        subscriber1._values = []
        subscriber2._values = []
        var.setValue(
            np.array([[4711]]),
            PdComWrapper.ScalarSelector(write_index),
        )
        next_value[write_index] = 4711

        self._client.readUntil(lambda s: len(s._values) > 0, context=subscriber1)
        self._client.readUntil(lambda s: len(s._values) > 0, context=subscriber2)

        self.assertTrue((subscriber1._values[-1][1] == next_value).all())
        expected1 = np.asarray(param(self._server))
        self.assertTrue((expected1 == next_value).all())
        self.assertEqual(subscriber2._values[-1][1][0], next_value[read_index])

        sub1 = subscriber1._subscription = None
        sub2 = subscriber2._subscription = None


class PdCom5Py(unittest.TestCase, PdCom5PyBase):
    def setUp(self):
        def socket_factory():
            sock = socket(AF_INET, SOCK_STREAM)
            sock.connect(("127.0.0.1", 2345))
            sock.settimeout(2)
            return sock

        self._doSetUp(config_dir + "/pdserv.conf", socket_factory)

    def tearDown(self):
        self._doTearDown()


class PdCom5PyTls(unittest.TestCase, PdCom5PyBase):
    def setUp(self):
        host = "localhost"

        def socket_factory():
            raw_sock = socket(AF_INET, SOCK_STREAM)
            ctx = ssl.SSLContext(ssl.PROTOCOL_TLS_CLIENT)
            ctx.load_verify_locations(cert_dir + "/ca.pem")
            ctx.verify_mode = ssl.CERT_REQUIRED
            ctx.check_hostname = True
            sock = ctx.wrap_socket(raw_sock, server_hostname=host)
            sock.connect((host, 4523))
            sock.settimeout(2)
            return sock

        self._doSetUp(config_dir + "/pdserv.tls-simple.conf", socket_factory)

    def tearDown(self):
        self._doTearDown()


class PdCom5PyTlsClientAuth(unittest.TestCase, PdCom5PyBase):
    def setUp(self):
        host = "localhost"

        def socket_factory():
            raw_sock = socket(AF_INET, SOCK_STREAM)
            ctx = ssl.SSLContext(ssl.PROTOCOL_TLS_CLIENT)
            ctx.load_verify_locations(cert_dir + "/ca.pem")
            ctx.load_cert_chain(cert_dir + "/client1.pem", cert_dir + "/client1.key")
            ctx.verify_mode = ssl.CERT_REQUIRED
            ctx.check_hostname = True
            sock = ctx.wrap_socket(raw_sock, server_hostname=host)
            sock.connect((host, 4523))
            sock.settimeout(2)
            return sock

        self._doSetUp(config_dir + "/pdserv.tls-clientauth.conf", socket_factory)

    def tearDown(self):
        self._doTearDown()
