import numpy as np  # this goes first, otherwise pdcom5 may not load properly
import asyncio
import datetime
from pdcom5 import (
    Process,
    InvalidArgument,
    LoginFailed,
    LogLevel,
    ScalarSelector,
    Transmission,
)
from pdcom5 import Exception as PdComException
from conf import cert_dir, config_dir
from pditserver.current import TestServer as Server, Loglevel as CppLoglevel
import sys
import ssl
from time import monotonic

try:
    from unittest import IsolatedAsyncioTestCase as TestCase

    def async_test(f):
        async def wrapper(testcase):
            try:
                await testcase._doSetUp()
                await f(testcase)
            finally:
                testcase._doTearDown()

        return wrapper

except ImportError:
    from unittest import TestCase

    def async_test(f):
        def wrapper(testcase):
            async def coro():
                try:
                    await testcase._doSetUp()
                    await f(testcase)
                finally:
                    testcase._doTearDown()

            loop = asyncio.get_event_loop()
            loop.run_until_complete(coro())

        return wrapper


class PdCom5PyBase:
    async def _doSetUp(self):
        self._server = Server()
        self._server.set_config_file_path(self.config_path)
        self._server.prepare()
        self._server.start()
        self._client = Process("Integration Tests")
        await self._client.connect(*self.connect_args())

    def _doTearDown(self):
        self._client.close()
        self._client = None
        if self._server is not None:
            self._server.clear()
            self._server = None
        print("teardown has finished", file=sys.stderr)

    @async_test
    async def test_server_version_name(self):
        self.assertEqual(self._client.name, "TestServer")
        self.assertEqual(self._client.version, "1.2.3")

    @async_test
    async def test_poll_eof_throws(self):
        var = await self._client.find("/dir1/param01")
        self.assertTrue(var is not None)
        self._server.clear()
        self._server = None
        with self.assertRaises((Exception, PdComException)):
            await var.poll()

    @async_test
    async def test_client_statistics(self):
        stats_list = await self._client.getClientStatistics()
        self.assertTrue(len(stats_list) == 1, "stats not empty")
        self.assertEqual(stats_list[0].application_name, "Integration Tests")
        self.assertNotEqual(len(stats_list[0].name), 0)
        self.assertNotEqual(stats_list[0].received_bytes, 0)
        self.assertNotEqual(stats_list[0].sent_bytes, 0)
        self.assertTrue(isinstance(stats_list[0].connected_time, datetime.timedelta))

    async def _do_check_broadcast(
        self, message: str, attribute: str, user: str = "anonymous"
    ):
        event = asyncio.Event()

        async def task():
            event.set()
            async for msg in self._client.streamBroadcasts():
                return msg

        task = asyncio.get_event_loop().create_task(task())
        await event.wait()
        try:
            await self._client.broadcast(message, attribute)
            m = await task
            self.assertEqual(m.message, message)
            self.assertEqual(m.attribute_name, attribute)
            self.assertEqual(m.user, user)
            self.assertLess(abs(m.time.total_seconds() - monotonic()), 2)
        finally:
            if not task.done():
                task.cancel()

    @async_test
    async def test_broadcasts(self):
        await self._do_check_broadcast("My'Message\"With&Special<chars>", "text")
        await self._do_check_broadcast("My'Message\"With&Special<chars>", "action")

        with self.assertRaises(InvalidArgument):
            await self._do_check_broadcast("My\0Message", "text")

    @async_test
    async def test_find(self):
        var1 = await self._client.find("/dir1/signal2")
        self.assertEqual(var1.path, "/dir1/signal2")
        self.assertEqual(var1.name, "signal2")
        self.assertEqual(var1.shape, (4,))

        # create background task to find another variable concurrently
        async def background():
            var2 = await self._client.find("/nonexistent")
            self.assertTrue(var2 is None)

        task = asyncio.get_event_loop().create_task(background())
        await asyncio.sleep(0)  # yield to background task
        var4 = await self._client.find("/dir1/signal2")
        self.assertTrue(var4 is not None)
        self.assertEqual(var4.path, "/dir1/signal2")
        await task

    @async_test
    async def test_ListAll(self):
        ans = await self._client.list()
        self.assertEqual(len(ans.directories), 0, "No dirs")
        expected_vars = (
            "/dir1/signal1",
            "/dir1/signal2",
            "/dir1/param01",
            "/dir1/param02",
            "/dir1/param03",
            "/operation/hour/counter/running",
            "/operation/hour/counter/restore",
        )
        found_vars = [var.path for var in ans.variables]
        for var in expected_vars:
            self.assertTrue(var in found_vars, "Variable %s found" % var)

    @async_test
    async def test_ListDirectory(self):
        ans = await self._client.list("/operation")
        self.assertEqual(len(ans.directories), 1)
        self.assertEqual(ans.directories[0], "/operation/hour")
        self.assertEqual(len(ans.variables), 0)

    @async_test
    async def test_ListVarsDirectories(self):
        ans = await self._client.list("/operation/hour/counter")
        self.assertEqual(len(ans.directories), 0)
        expected_vars = (
            "/operation/hour/counter/running",
            "/operation/hour/counter/restore",
        )
        found_vars = [var.path for var in ans.variables]
        for var in expected_vars:
            self.assertTrue(var in found_vars, "Variable %s found" % var)

    @async_test
    async def test_multidimension(self):
        var1 = await self._client.find("/dir1/param03")
        self.assertEqual(var1.shape, (2, 3))
        my_index = (0, 1)
        sub1 = await self._client.subscribe(Transmission.event_mode, var1)
        sub2 = await self._client.subscribe(
            Transmission.event_mode, var1, ScalarSelector(my_index)
        )
        val1_expected = np.asarray(((2, 3, 4), (5, 6, 7)))
        val1_rowwise_expected = [2, 3, 4, 5, 6, 7]
        await var1.setValue(val1_expected)
        val1_actual, val1_ts = await sub1.read()
        val1s_actual = await sub2.poll()
        val1_rowwise_actual = [x for x in sub1]
        val1_server = np.array(self._server.param3.value)
        self.assertNotEqual(val1_ts, datetime.timedelta(0))
        self.assertTrue((val1_actual == val1_expected).all())
        self.assertTrue((val1_server == val1_expected).all())
        self.assertEqual(val1s_actual, val1_expected[my_index])
        self.assertEqual(
            val1_rowwise_expected, val1_rowwise_actual, "iterator interface"
        )
        set_index = (0, 1)
        await var1.setValue(12, ScalarSelector(set_index))
        val2_actual, _ = await sub1.read()
        val1_expected[set_index] = 12
        val1_server = np.array(self._server.param3.value)
        self.assertTrue((val2_actual == val1_expected).all())
        self.assertTrue((val1_server == val1_expected).all())
        self.assertEqual(sub2.value, val1_expected[my_index])

        try:
            await var1.setValue([1] * 100)
            self.assertTrue(False, "Range error must be caught")
        except Exception:
            pass

        try:
            await var1.setValue(False, 100)
            self.assertTrue(False, "Offset error must be caught")
        except Exception:
            pass

        y = np.reshape(np.arange(16), (4, 4))
        view = y[1:3, 1:4]
        self.assertEqual(view.shape, var1.shape)
        self.assertFalse(view.flags["C_CONTIGUOUS"], "test for non-contignuous data")
        try:
            await var1.setValue(view)
            self.assertTrue(False, "non-contiguous data must be rejected")
        except Exception:
            pass

    @async_test
    async def test_periodic_subscription(self):
        var = await self._client.find("/dir1/signal1")
        decimation = 10
        period = Server.task1_period * decimation
        subscriber = self._client.create_subscriber(period)
        sub = await subscriber.subscribe(var)
        values = []
        for _ in range(5):
            values.append(await sub.read())
        sub.cancel()

        for i, j in zip(values[:-1], values[1:]):
            self.assertAlmostEqual(
                (i[1] + period).total_seconds(),
                j[1].total_seconds(),
                delta=Server.task1_period.total_seconds(),
            )
            self.assertEqual(i[0] + decimation, j[0])

    @async_test
    async def test_decimation_periodic_subscription(self):
        # signal registered in pdserv with decimation of 2.
        var = await self._client.find("/dir1/signal12")
        decimation = 10
        period = Server.task1_period * decimation * 2
        subscriber = self._client.create_subscriber(period)
        sub = await subscriber.subscribe(var)
        values = []
        for _ in range(5):
            values.append(await sub.read())
        sub.cancel()

        for i, j in zip(values[:-1], values[1:]):
            self.assertAlmostEqual(
                (i[1] + period).total_seconds(),
                j[1].total_seconds(),
                delta=Server.task1_period.total_seconds(),
            )
            self.assertEqual(i[0] + decimation * 2, j[0])

    @async_test
    async def test_use_subscription_in_multiple_tasks(self):
        var = await self._client.find("/dir1/signal1")
        decimation = 1
        period = Server.task1_period * decimation
        subscriber = self._client.create_subscriber(period)
        sub = await subscriber.subscribe(var)

        ping = asyncio.Event()
        pong = asyncio.Event()

        async def test_task():
            await ping.wait()
            exception_raised = False
            try:
                async with subscriber.newValues():
                    pass
            except Exception:
                exception_raised = True
            pong.set()
            return exception_raised

        task = asyncio.get_event_loop().create_task(test_task())

        async with subscriber.newValues():
            ping.set()
            await pong.wait()

        # awaiting the same subscription in two tasks concurrently is
        # not supported
        self.assertTrue(await task)
        sub.cancel()

    @async_test
    async def test_fast_periodic_subscription_old_api(self):
        var = await self._client.find("/dir1/signal1")
        decimation = 1
        period = Server.task1_period * decimation
        subscriber = self._client.create_subscriber(period)
        sub = await subscriber.subscribe(var)
        values = []
        for _ in range(5):
            values.append(await sub.read())
        sub.cancel()

        for i, j in zip(values[:-1], values[1:]):
            self.assertEqual(i[0] + decimation, j[0])

    @async_test
    async def test_block_subscription_old_api(self):
        decimation = 2
        period = Server.task1_period * decimation
        subscriber = self._client.create_subscriber(period)
        sub1 = await subscriber.subscribe("/dir1/signal1")
        sub2 = await subscriber.subscribe("/dir1/signal11")
        values = []
        with self.assertWarns(FutureWarning):
            for _ in range(20):
                async with subscriber.newValues() as ts:
                    values.append((ts, sub1.value, sub2.value))

        for i, j in zip(values[:-1], values[1:]):
            # timestamp must be monotonic
            self.assertLess(i[0], j[0])
            # values as well
            self.assertEqual(i[1] + decimation, j[1])
            self.assertEqual(i[2] + decimation, j[2])
            # both signals have constant offset
            self.assertEqual(i[1] + 10, i[2])

    @async_test
    async def test_block_subscription_new_api(self):
        decimation = 2
        period = Server.task1_period * decimation
        subscriber = self._client.create_subscriber(period)
        sub1 = await subscriber.subscribe("/dir1/signal1")
        sub2 = await subscriber.subscribe("/dir1/signal11")
        # also ensure subscription reuse
        for _ in range(3):
            values = []
            num_vals = 20
            samples = 0
            ts_gen = subscriber.newValues(autoclose=False)
            try:
                async for ts in ts_gen:
                    values.append((ts, sub1.value, sub2.value))
                    samples += 1
                    if samples >= num_vals:
                        break
            finally:
                await ts_gen.aclose()

            self.assertEqual(len(values), num_vals)

            for i, j in zip(values[:-1], values[1:]):
                # timestamp must be monotonic
                self.assertLess(i[0], j[0])
                # values as well
                self.assertEqual(i[1] + decimation, j[1])
                self.assertEqual(i[2] + decimation, j[2])
                # both signals have constant offset
                self.assertEqual(i[1] + 10, i[2])

    @async_test
    async def test_fast_periodic_subscription_new_api(self):
        var = await self._client.find("/dir1/signal1")
        decimation = 1
        period = Server.task1_period * decimation
        subscriber = self._client.create_subscriber(period)
        sub = await subscriber.subscribe(var)
        count = 0
        values = []
        num_values = 100
        async for ts in subscriber.newValues():
            values.append((sub.value, ts))
            count += 1
            if count >= num_values:
                break
        sub.cancel()
        self.assertEqual(len(values), num_values)

        for i, j in zip(values[:-1], values[1:]):
            self.assertEqual(i[0] + decimation, j[0])

    @async_test
    async def test_vector_events(self):
        msg1 = await self._client.getMessage(4711)
        self.assertTrue(msg1 is None, "Unknown message is none")
        active_1 = await self._client.activeMessages()
        self.assertEqual(len(active_1), 0, "no active messages on startup")
        self._server.set_event(1, CppLoglevel.alert)
        await asyncio.sleep(0.2)
        active_2 = await self._client.activeMessages()
        self.assertEqual(len(active_2), 1)
        msg1 = active_2[0]
        msg2 = await self._client.getMessage(msg1.seqNo)
        self.assertTrue(msg2 is not None, "Message was found by seqNo")
        for msg in (msg1, msg2):
            self.assertEqual(msg.index, 1)
            self.assertEqual(msg.path, "/Event1")
            self.assertEqual(msg.text, "Second Index")
            self.assertEqual(msg.level, LogLevel.Alert)
        self._server.reset_event(1)
        await asyncio.sleep(0.4)
        active_3 = await self._client.activeMessages()
        self.assertEqual(len(active_3), 1)
        self.assertEqual(active_3[0].level, LogLevel.Reset, "Message was reset")

    @async_test
    async def test_single_events(self):
        msg1 = await self._client.getMessage(4711)
        self.assertTrue(msg1 is None, "Unknown message is none")
        self._server.set_single_event(CppLoglevel.alert)
        with self.assertWarns(FutureWarning):
            process_msg1 = await self._client.pollMessage()
        active_msg1 = await self._client.activeMessages()
        self.assertEqual(len(active_msg1), 1, "one active message")
        for msg, txt in ((process_msg1, "process"), (active_msg1[0], "active")):
            self.assertEqual(msg.index, -1, "message received by " + txt)
            self.assertEqual(msg.path, "/Event2")
            self.assertEqual(msg.text, "Single Event")
            self.assertEqual(msg.level, LogLevel.Alert)
        self._server.reset_single_event()
        with self.assertWarns(FutureWarning):
            process_msg2 = await self._client.pollMessage()
        # active message should return one with level == Reset
        active_msg2 = await self._client.activeMessages()
        self.assertEqual(len(active_msg2), 1, "one active messages")
        for msg, txt in ((process_msg2, "process"), (active_msg2[0], "active")):
            self.assertEqual(msg.index, -1, "reset message received")
            self.assertEqual(msg.path, "/Event2")
            self.assertEqual(msg.level, LogLevel.Reset)


class AsyncIoTest(TestCase, PdCom5PyBase):
    def __init__(self, *args):
        super().__init__(*args)
        self.config_path = config_dir + "/pdserv.conf"

    def connect_args(self):
        return ("msr://localhost",)


class AsyncIoSaslTest(TestCase, PdCom5PyBase):
    def __init__(self, *args):
        super().__init__(*args)
        self.config_path = config_dir + "/pdserv.sasl.conf"

    def connect_args(self):
        return (r"msr://bob%40example.com:Bob!@localhost",)

    @async_test
    async def test_login_failed(self):
        self._client.close()
        with self.assertRaises(LoginFailed):
            await self._client.connect(r"msr://bob%40example.com:1nValid@localhost")


class AsyncIoTlsTest(TestCase, PdCom5PyBase):
    def __init__(self, *args):
        super().__init__(*args)
        self.config_path = config_dir + "/pdserv.tls-simple.conf"

    def connect_args(self):
        ctx = ssl.SSLContext(ssl.PROTOCOL_TLS_CLIENT)
        ctx.load_verify_locations(cert_dir + "/ca.pem")
        ctx.verify_mode = ssl.CERT_REQUIRED
        ctx.check_hostname = True
        return ("msrs://localhost", ctx)


class AsyncIoTlsClientAuthTest(TestCase, PdCom5PyBase):
    def __init__(self, *args):
        super().__init__(*args)
        self.config_path = config_dir + "/pdserv.tls-clientauth.conf"

    def connect_args(self):
        ctx = ssl.SSLContext(ssl.PROTOCOL_TLS_CLIENT)
        ctx.load_verify_locations(cert_dir + "/ca.pem")
        ctx.load_cert_chain(cert_dir + "/client1.pem", cert_dir + "/client1.key")
        ctx.verify_mode = ssl.CERT_REQUIRED
        ctx.check_hostname = True
        return ("msrs://localhost", ctx)
