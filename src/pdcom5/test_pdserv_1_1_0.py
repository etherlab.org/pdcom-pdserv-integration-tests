import numpy  # noqa: F401

# numpy goes first, otherwise pdcom5 may not load properly
import asyncio
from pdcom5 import (
    LogLevel,
    LoginFailed,
    InvalidSubscription,
    Process,
    Transmission,
)
from pdcom5 import Exception as PdComException
from conf import config_dir
from pditserver.v1_1_0 import TestServer as Server
from pditserver.v1_1_0 import Loglevel as CppLoglevel
import sys
from datetime import timedelta

try:
    from unittest import IsolatedAsyncioTestCase as TestCase

    def async_test(f):
        async def wrapper(testcase):
            try:
                await testcase._doSetUp()
                await f(testcase)
            finally:
                testcase._doTearDown()

        return wrapper

except ImportError:
    from unittest import TestCase

    def async_test(f):
        def wrapper(testcase):
            async def coro():
                try:
                    await testcase._doSetUp()
                    await f(testcase)
                finally:
                    testcase._doTearDown()

            loop = asyncio.get_event_loop()
            loop.run_until_complete(coro())

        return wrapper


class PdServ110Tests(TestCase):
    async def _doSetUp(self):
        config_path = config_dir + "/pdserv.conf"
        self._server = Server()
        self._server.set_config_file_path(config_path)
        self._server.prepare()
        self._server.start()
        self._client = Process("Integration Tests")
        connected = False
        retries = 10
        # prepare returns before servers started,
        # so retry a couple of times
        while not connected:
            try:
                await asyncio.sleep(0.5)
                await self._client.connect("msr://localhost")
                connected = True
            except OSError:
                retries -= 1
                if retries == 0:
                    raise

    def _doTearDown(self):
        self._client.close()
        self._client = None
        if self._server is not None:
            self._server.clear()
            self._server = None
        print("teardown has finished", file=sys.stderr)

    @async_test
    async def test_server_version_name(self):
        self.assertEqual(self._client.name, "TestServer")
        self.assertEqual(self._client.version, "1.2.3")

    @async_test
    async def test_poll_eof_throws(self):
        var = await self._client.find("/dir1/param01")
        self.assertTrue(var is not None)
        self._server.clear()
        self._server = None
        with self.assertRaises((Exception, PdComException)):
            await var.poll()

    @async_test
    async def test_find(self):
        var1 = await self._client.find("/dir1/signal2")
        self.assertEqual(var1.path, "/dir1/signal2")
        self.assertEqual(var1.name, "signal2")
        self.assertEqual(var1.shape, (4,))

        # create background task to find another variable concurrently
        async def background():
            var2 = await self._client.find("/nonexistent")
            self.assertTrue(var2 is None)

        task = asyncio.get_event_loop().create_task(background())
        await asyncio.sleep(0)  # yield to background task
        var4 = await self._client.find("/dir1/signal2")
        self.assertTrue(var4 is not None)
        self.assertEqual(var4.path, "/dir1/signal2")
        await task

    @async_test
    async def test_ListAll(self):
        ans = await self._client.list()
        self.assertEqual(len(ans.directories), 0, "No dirs")
        expected_vars = (
            "/dir1/signal1",
            "/dir1/signal2",
            "/dir1/param01",
            "/dir1/param02",
            "/dir1/param03",
            "/operation/hour/counter/running",
            "/operation/hour/counter/restore",
        )
        found_vars = [var.path for var in ans.variables]
        for var in expected_vars:
            self.assertTrue(var in found_vars, "Variable %s found" % var)

    @async_test
    async def test_ListDirectory(self):
        ans = await self._client.list("/operation")
        self.assertEqual(len(ans.directories), 1)
        self.assertEqual(ans.directories[0], "/operation/hour")
        self.assertEqual(len(ans.variables), 0)

    @async_test
    async def test_ListVarsDirectories(self):
        ans = await self._client.list("/operation/hour/counter")
        self.assertEqual(len(ans.directories), 0)
        expected_vars = (
            "/operation/hour/counter/running",
            "/operation/hour/counter/restore",
        )
        found_vars = [var.path for var in ans.variables]
        for var in expected_vars:
            self.assertTrue(var in found_vars, "Variable %s found" % var)

    @async_test
    async def DISABLED_test_ParameterEcho(self):
        # signal5 := param05 in rt task
        sig5 = await self._client.find("/dir1/signal5")
        sig5sub = await self._client.subscribe(Transmission.event_mode, sig5)
        param5 = await self._client.find("/dir1/param05")
        param5sub = await self._client.subscribe(Transmission.event_mode, param5)
        # add extra poll to drain zero values
        await sig5sub.poll()
        job1_sync = asyncio.Event()
        job2_sync = asyncio.Event()

        async def sig_event_job():
            job1_sync.set()
            value, _ = await sig5sub.read()
            self.assertEqual(value, 4711)

        async def param_event_job():
            job2_sync.set()
            value, _ = await param5sub.read()
            self.assertEqual(value, 4711)

        sig5task = asyncio.get_event_loop().create_task(sig_event_job())
        param5task = asyncio.get_event_loop().create_task(param_event_job())

        # make sure tasks have been startet
        await job1_sync.wait()
        await job2_sync.wait()
        await param5.setValue(4711)
        await param5task
        await sig5task
        value = await sig5sub.poll()
        self.assertEqual(value, 4711)

    async def _set_and_poll_event(self, level):
        event = asyncio.Event()

        async def poll():
            event.set()
            return await self._client.pollMessage()

        task = asyncio.get_event_loop().create_task(poll())
        await event.wait()
        self._server.set_single_event(level)
        return await task

    @async_test
    async def test_single_events(self):
        msg1 = await self._client.getMessage(4711)
        self.assertTrue(msg1 is None, "getMessage does not work")
        process_msg1 = await self._set_and_poll_event(CppLoglevel.alert)
        active_msg1 = await self._client.activeMessages()
        self.assertEqual(len(active_msg1), 0, "getActiveMessages does not work")
        self.assertEqual(process_msg1.level, LogLevel.Error)
        self.assertEqual(process_msg1.path, "/Event2")
        self.assertEqual(process_msg1.text, "Single Event")
        self._server.set_single_event(CppLoglevel.reset)
        process_msg2 = await self._set_and_poll_event(CppLoglevel.reset)
        self.assertEqual(process_msg2.level, LogLevel.Reset)

    @async_test
    async def test_no_login(self):
        has_thrown = False
        try:
            await self._client.login("bob@example.com", "Bob!")
        except LoginFailed:
            has_thrown = True
        self.assertTrue(has_thrown)

    @async_test
    async def test_periodic_subscription(self):
        """test timestamp interpolation"""
        signal = "/dir1/signal1"
        sub = await self._client.subscribe(0.1, signal)
        value_pairs = [await sub.read() for _ in range(5)]
        # event transmission not possible right now
        sub2_succeeded = False
        try:
            _ = await self._client.subscribe(Transmission.event_mode, signal)
            sub2_succeeded = True
        except InvalidSubscription:
            pass
        self.assertFalse(sub2_succeeded)
        # check interpolation
        for i in range(len(value_pairs) - 1):
            ms = (value_pairs[i + 1][1] - value_pairs[i][1]).microseconds
            self.assertGreater(ms, 50_000)
            self.assertLess(ms, 150_000)
            self.assertEqual(value_pairs[i + 1][0], value_pairs[i][0] + 10)

    @async_test
    async def test_multiple_periods(self):
        # make some background noise with 0.5 Hz
        slow_background_sub = await self._client.subscribe(0.5, "/dir1/signal5")
        finished = False

        async def back():
            old_ts = (await slow_background_sub.read())[1]
            while not finished:
                new_ts = (await slow_background_sub.read())[1]
                ms = (new_ts - old_ts).microseconds / 1000
                self.assertGreater(ms, 400)
                self.assertLess(ms, 800)
                old_ts = new_ts

        back_task = asyncio.get_event_loop().create_task(back())

        signal = "/dir1/signal1"
        sub01 = await self._client.subscribe(1.0, signal)
        value_pairs = [await sub01.read() for _ in range(5)]
        # check interpolation
        for i in range(len(value_pairs) - 1):
            delta = value_pairs[i + 1][1] - value_pairs[i][1]
            self.assertGreater(delta, timedelta(milliseconds=800))
            self.assertLess(delta, timedelta(milliseconds=1200))
            self.assertEqual(value_pairs[i + 1][0], value_pairs[i][0] + 100)
        sub01.cancel()

        # now switch to faster subscription
        sub001 = await self._client.subscribe(0.1, signal)
        value_pairs = [await sub001.read() for _ in range(20)]
        # check interpolation
        for i in range(len(value_pairs) - 1):
            delta = value_pairs[i + 1][1] - value_pairs[i][1]
            self.assertGreater(delta, timedelta(milliseconds=80))
            self.assertLess(delta, timedelta(milliseconds=120))
            self.assertEqual(value_pairs[i + 1][0], value_pairs[i][0] + 10)
        sub001.cancel()

        finished = True
        await back_task
