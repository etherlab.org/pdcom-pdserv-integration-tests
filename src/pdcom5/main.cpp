/*****************************************************************************
 *
 * Copyright (C) 2021 Bjarne von Horn (vh at igh dot de)
 *
 * This file is part of the PdCom library.
 *
 * The PdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The PdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the PdCom library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

#include "../server.h"

#include <chrono>
#include <conf.h>
#include <errno.h>
#include <fstream>
#include <gtest/gtest.h>
#include <memory>
#include <netinet/in.h>
#include <pdcom5/Exception.h>
#include <pdcom5/MessageManagerBase.h>
#include <pdcom5/PosixProcess.h>
#include <pdcom5/Process.h>
#include <pdcom5/SecureProcess.h>
#include <pdcom5/SimpleLoginManager.h>
#include <pdcom5/Subscriber.h>
#include <pdcom5/Subscription.h>
#include <queue>
#include <stdexcept>
#include <sys/socket.h>
#include <sys/timerfd.h>


using LoginResult = PdCom::SimpleLoginManager::LoginResult;

struct MyLoginManager : PdCom::SimpleLoginManager
{
    enum class NextAction {
        Cancel,
        Throw,
        Alice,
        Bob,
        Unknown,
        Ernie,
    };
    std::queue<NextAction> next_action_;
    std::vector<LoginResult> completed_;

    struct LMEx : std::exception
    {};

    using PdCom::SimpleLoginManager::SimpleLoginManager;
    using SimpleLoginManager::login;
    using SimpleLoginManager::logout;

    std::string getAuthname() override
    {
        if (next_action_.empty()) {
            ADD_FAILURE() << "No action available";
            throw Cancel();
        }
        auto na = next_action_.front();
        next_action_.pop();
        switch (na) {
            case NextAction::Cancel:
                throw Cancel();
            case NextAction::Alice:
                return "alice@example.com";
            case NextAction::Bob:
                return "bob@example.com";
            case NextAction::Unknown:
                return "eve@example.com";
            case NextAction::Ernie:
                return "ernie";
            default:
                throw LMEx();
        }
    }

    std::string getPassword() override
    {
        if (next_action_.empty()) {
            ADD_FAILURE() << "No action available";
            throw Cancel();
        }
        auto na = next_action_.front();
        next_action_.pop();
        switch (na) {
            case NextAction::Cancel:
                throw Cancel();
            case NextAction::Alice:
                return "Alice!";
            case NextAction::Bob:
                return "Bob!";
            case NextAction::Unknown:
                return "1nsecure";
            case NextAction::Ernie:
                return "1nSecure";
            default:
                throw LMEx();
        }
    }

    void completed(LoginResult success) override
    {
        completed_.push_back(success);
    }
};

struct MyMessageManager : public PdCom::MessageManagerBase
{
    using MessageManagerBase::MessageManagerBase;

    using MessageManagerBase::activeMessages;
    using MessageManagerBase::getMessage;

    void processMessage(PdCom::Message message) override
    {
        process_message_.push_back(message);
    }

    void getMessageReply(PdCom::Message message) override
    {
        get_message_reply_.push_back(message);
    }

    void activeMessagesReply(std::vector<PdCom::Message> messageList) override
    {
        active_messages_reply_.push_back(messageList);
    }

    std::vector<PdCom::Message> process_message_, get_message_reply_;
    std::vector<std::vector<PdCom::Message>> active_messages_reply_;
};

struct MyBroadcast
{
    std::string message_, attribute_, user_;
    std::chrono::nanoseconds time_ns_;
};

template <typename Parent>
class MyProcessBase : public Parent, public PdCom::PosixProcess
{
  public:
    using PdCom::Process::find;
    using PdCom::Process::list;
    using PdCom::Process::reset;
    using PdCom::Process::setAuthManager;
    using PdCom::Process::setMessageManager;


    static constexpr auto recv_timeout = std::chrono::seconds {1};
    struct timeout : std::runtime_error
    {
        timeout() : std::runtime_error("read() timeout") {}
    };

    template <typename... Args>
    MyProcessBase(const char *host, unsigned short port, Args &&...args) :
        Parent(std::forward<Args>(args)...), PdCom::PosixProcess(host, port)
    {}

    void readWithTimeout();
    template <typename T>
    ::testing::AssertionResult readUntil(T &&predicate, int max_tries = 5)
    {
        Parent::callPendingCallbacks();
        if (predicate())
            return ::testing::AssertionSuccess();
        for (int i = 0; i < max_tries; ++i) {
            try {
                readWithTimeout();
            }
            catch (timeout const &) {
            }
            if (predicate())
                return ::testing::AssertionSuccess();
        }
        return ::testing::AssertionFailure();
    }

    void connect()
    {
        if (!readUntil([this]() { return this->connected_; }, 15))
            throw timeout {};
    }

    void connected() override { connected_ = true; }
    void findReply(const PdCom::Variable &var) override
    {
        found_vars_.push_back(var);
    }

    void listReply(
            std::vector<PdCom::Variable> variables,
            std::vector<std::string> dirs) override
    {
        list_replies_.push_back({dirs, variables});
    }

    void broadcastReply(
            const std::string &message,
            const std::string &attr,
            std::chrono::nanoseconds time_ns,
            const std::string &user) override
    {
        broadcasts_.push_back({message, attr, user, time_ns});
    }

    template <
            typename Exception,
            typename... Result,
            class Callback =
                    std::function<::testing::AssertionResult(Result...)>>
    ::testing::AssertionResult synchronize(
            PdCom::Future<Exception, Result...> future,
            Callback &&callback = {})
    {
        ::testing::AssertionResult res = ::testing::AssertionSuccess();
        if (future.empty())
            return res;
        bool success = false, error = false;
        future.then([&success, callback, &res](Result... result) {
                  success = true;
                  if constexpr (std::is_convertible_v<Callback, bool>)
                      if (!callback)
                          return;
                  res = callback(std::forward<Result>(result)...);
              }).handle_exception([&error](const PdCom::Exception &) {
            error = true;
        });
        const auto ru =
                readUntil([&success, &error] { return success || error; });
        if (!ru)
            return ru;
        if (error)
            return ::testing::AssertionFailure();
        return res;
    }

    bool running_   = true;
    bool connected_ = false;
    std::vector<PdCom::Variable> found_vars_;
    std::vector<MyBroadcast> broadcasts_;

    struct ListReply
    {
        std::vector<std::string> dirs;
        std::vector<PdCom::Variable> vars;
    };

    std::vector<ListReply> list_replies_;
};

template <typename P>
void MyProcessBase<P>::readWithTimeout()
{
    timeval tout {toTimeval(recv_timeout)};
    fd_set fds;
    FD_ZERO(&fds);
    FD_SET(PdCom::PosixProcess::fd_, &fds);
    switch (select(
            PdCom::PosixProcess::fd_ + 1, &fds, nullptr, nullptr, &tout)) {
        case 0:
            throw timeout {};
        case 1:
            PdCom::Process::asyncData();
            break;
        default:
            throw std::runtime_error("select() failed");
    }
}


class MyProcess : public MyProcessBase<PdCom::Process>
{
  public:
    MyProcess(const char *host) : MyProcessBase<PdCom::Process>(host, 2345) {}
    int read(char *buf, int count) override
    {
        const int ans = posixRead(buf, count);
        if (ans == 0)
            running_ = false;
        return ans;
    }
    void write(const char *buf, size_t count) override
    {
        posixWriteBuffered(buf, count);
    }


    void flush() override { posixFlush(); }

    static constexpr const char *conf_name = "/pdserv.conf";
};

class SaslProcess : public MyProcess
{
  public:
    using MyProcess::MyProcess;

    static constexpr const char *conf_name = "/pdserv.sasl.conf";
    ::testing::AssertionResult do_login(
            MyLoginManager &lm,
            MyLoginManager::NextAction a1,
            MyLoginManager::NextAction a2,
            LoginResult expected,
            bool call_login)
    {
        lm.completed_.clear();
        lm.next_action_ = {};
        lm.next_action_.push(a1);
        lm.next_action_.push(a2);
        if (call_login and !lm.login())
            return ::testing::AssertionFailure() << "Auth not available";
        const auto ru = readUntil([&lm] { return !lm.completed_.empty(); });
        if (!ru)
            return ru;
        if (lm.completed_.size() != 1)
            return ::testing::AssertionFailure()
                    << "Recieved " << lm.completed_.size()
                    << " login status updates, expected 1";
        if (lm.completed_.back() != expected)
            return ::testing::AssertionFailure()
                    << "Login was "
                    << (expected == LoginResult::Success
                                ? " not successful"
                                : " succesful despite wrong password");
        return ::testing::AssertionSuccess();
    }

    ::testing::AssertionResult
    login_ok(MyLoginManager &lm, bool call_login = true)
    {
        return do_login(
                lm, MyLoginManager::NextAction::Alice,
                MyLoginManager::NextAction::Alice, LoginResult::Success,
                call_login);
    }
    ::testing::AssertionResult
    login_wrong_password(MyLoginManager &lm, bool call_login = true)
    {
        return do_login(
                lm, MyLoginManager::NextAction::Alice,
                MyLoginManager::NextAction::Bob, LoginResult::Error,
                call_login);
    }
    ::testing::AssertionResult
    login_unknown_user(MyLoginManager &lm, bool call_login = true)
    {
        return do_login(
                lm, MyLoginManager::NextAction::Unknown,
                MyLoginManager::NextAction::Unknown, LoginResult::Error,
                call_login);
    }
};

static PdCom::SecureProcess::EncryptionDetails
getED(const char *host, const char *client_certs = nullptr)
{
    const auto load_file = [](const std::string &f) {
        std::ifstream if_f(f);
        std::string b;
        getline(if_f, b, '\0');
        return b;
    };
    return {
            load_file(std::string(cert_dir) + "/ca.pem"),
            host,
            client_certs
                    ? load_file(std::string(cert_dir) + client_certs + ".pem")
                    : "",
            client_certs
                    ? load_file(std::string(cert_dir) + client_certs + ".key")
                    : "",
    };
}

class MyTlsProcess : public MyProcessBase<PdCom::SecureProcess>
{
  public:
    MyTlsProcess(const char *host, const char *client_certs = nullptr) :
        MyProcessBase<PdCom::SecureProcess>(
                host,
                4523,
                getED(host, client_certs))
    {
        handshake();
    }
    int read(char *buf, int count) override { return posixRead(buf, count); }
    void write(const char *buf, size_t count) override
    {
        posixWriteDirect(buf, count);
    }

    static constexpr const char *conf_name = "/pdserv.tls-simple.conf";

    bool isEof() const
    {
        char c;
        return recv(fd_, &c, 1, MSG_PEEK | MSG_DONTWAIT) == 0;
    }
};

class MyTlsClientAuthProcess : public MyTlsProcess
{
  public:
    MyTlsClientAuthProcess(
            const char *host,
            const char *client_certs = "/client1") :
        MyTlsProcess(host, client_certs)
    {}
    static constexpr const char *conf_name = "/pdserv.tls-clientauth.conf";
};

class MySubscriber : public PdCom::Subscriber
{
  public:
    using PdCom::Subscriber::Subscriber;

    void newValues(std::chrono::nanoseconds time_ns) override
    {
        recieved_newvalues_.push_back(time_ns);
    }

    void stateChanged(PdCom::Subscription const &sub) override
    {
        recieved_states_.push_back(sub.getState());
    }

    std::vector<PdCom::Subscription::State> recieved_states_;
    std::vector<std::chrono::nanoseconds> recieved_newvalues_;
};

struct MyDoubleSubscriber : public PdCom::Subscriber
{
    std::vector<std::pair<std::chrono::nanoseconds, double>>
            recieved_newvalues_;
    std::vector<PdCom::Subscription::State> recieved_states_;

    PdCom::Subscription const &sub_;

    MyDoubleSubscriber(
            PdCom::Transmission const &t,
            PdCom::Subscription const &sub) :
        Subscriber(t), sub_(sub)
    {}
    void newValues(std::chrono::nanoseconds time_ns) override
    {
        double d;
        sub_.getValue(d);
        recieved_newvalues_.push_back({time_ns, d});
    }

    void stateChanged(PdCom::Subscription const &sub) override
    {
        if (&sub != &sub_) {
            FAIL() << "Subscription mismatch";
        }
        else {
            recieved_states_.push_back(sub_.getState());
        }
    }
};


template <typename Process>
class PdCom5TestBase : public ::testing::Test
{
  public:
    void SetUp() override
    {
        PdCom::SecureProcess::InitLibrary();
        if constexpr (std::is_same_v<Process, SaslProcess>)
            PdCom::SimpleLoginManager::InitLibrary();
        server_ = std::make_unique<TestServer>();
        server_->set_config_file_path(
                std::string(config_dir) + Process::conf_name);
        ASSERT_NO_THROW(server_->prepare());
        server_->start();
        client_ = std::make_unique<Process>("localhost");
        if constexpr (!std::is_same_v<Process, SaslProcess>)
            client_->connect();
    }
    void TearDown() override
    {
        client_.reset();
        if (server_)
            server_->stop();
        server_.reset();
        if constexpr (std::is_same_v<Process, SaslProcess>)
            PdCom::SimpleLoginManager::FinalizeLibrary();
        PdCom::SecureProcess::FinalizeLibrary();
    }
    static std::unique_ptr<TestServer> server_;
    static std::unique_ptr<Process> client_;

    static void _implCheckBroadcast(
            const std::string &message,
            const char *attribute,
            const char *user = "anonymous",
            int max_td       = 2);
    static void _implParameterEventSubscription();
    static void _implSplitVectorParameterEventSubscription();
    static void _implMultidimParameter();
    static void _implSignalPeriodicSubscription();
    static void _implSplitVectorSignalEventSubscription();

    template <typename SecondProcess>
    static void _implParameterUpdateViaSecondConnection();
};

template <typename P>
std::unique_ptr<TestServer> PdCom5TestBase<P>::server_;
template <typename P>
std::unique_ptr<P> PdCom5TestBase<P>::client_;

using PdCom5CppTests    = PdCom5TestBase<MyProcess>;
using PdCom5SaslTests   = PdCom5TestBase<SaslProcess>;
using PdCom5CppTlsTests = PdCom5TestBase<MyTlsProcess>;

using PdCom5CppTlsClientAuthTests = PdCom5TestBase<MyTlsClientAuthProcess>;

TEST(PdCom5CppTestsPlain, PdServNoPrepare)
{
    TestServer server;
    server.set_config_file_path(std::string(config_dir) + "/pdserv.conf");
}

class PortBlocker
{
    const int fd_;

  public:
    PortBlocker(unsigned short port) : fd_(socket(AF_INET, SOCK_STREAM, 0))
    {
        const std::string err_msg =
                "Blocking port " + std::to_string(port) + " failed.";
        if (fd_ != -1) {
            const int optval = 1;
            if (!setsockopt(
                        fd_, SOL_SOCKET, SO_REUSEADDR, &optval,
                        sizeof(optval))) {
                const sockaddr_in addr {
                        AF_INET, htons(port), htonl(INADDR_LOOPBACK)};
                if (!bind(fd_, reinterpret_cast<const sockaddr *>(&addr),
                          sizeof(addr))
                    and !listen(fd_, 5)) {
                    return;
                }
            }
            perror(err_msg.c_str());
            ::close(fd_);
        }
        throw std::runtime_error(err_msg);
    }

    ~PortBlocker() { ::close(fd_); }
};

TEST(PdCom5CppTestsPlain, PortInUse)
{
    const auto run = [](const char *config_file, unsigned short port1,
                        unsigned short port2) {
        PortBlocker const pb1(port1), pb2(port2);
        TestServer server;
        server.set_config_file_path(std::string(config_dir) + config_file);
        server.prepare();
    };

    EXPECT_THROW(
            (run("/pdserv.conf", 2345, 3340)), PdServ::ServBase::prepare_failed)
            << "MSR Port is in use, should throw";
    ASSERT_NO_THROW((run("/pdserv.conf", 3341, 3342)))
            << "MSR Port is now free, should not throw";

    EXPECT_THROW(
            (run("/pdserv.tls-simple.conf", 4523, 3343)),
            PdServ::ServBase::prepare_failed)
            << "Secure MSR Port is in use, should throw";
    ASSERT_NO_THROW((run("/pdserv.tls-simple.conf", 3344, 3345)))
            << "Secure MSR Port is now free, should not throw";


    EXPECT_THROW(
            (run("/pdserv.tls-simple.conf", 4523, 2345)),
            PdServ::ServBase::prepare_failed)
            << "Both Ports are in use, should throw";
    ASSERT_NO_THROW((run("/pdserv.tls-simple.conf", 3347, 3348)))
            << "Both Ports are nowfree, should not throw";
}

TEST(PdCom5CppTestsPlain, MissingConfigFile)
{
    TestServer server;
    server.set_config_file_path("/missing_config_file.yml");
    EXPECT_THROW(server.prepare(), PdServ::ServBase::prepare_failed);
}

TEST_F(PdCom5CppTests, ServerNameVersion)
{
    EXPECT_EQ(client_->name(), "TestServer");
    EXPECT_EQ(client_->version(), "1.2.3");
}


TEST_F(PdCom5CppTlsTests, ServerNameVersion)
{
    EXPECT_EQ(client_->name(), "TestServer");
    EXPECT_EQ(client_->version(), "1.2.3");
}

TEST_F(PdCom5CppTlsClientAuthTests, ServerNameVersion)
{
    EXPECT_EQ(client_->name(), "TestServer");
    EXPECT_EQ(client_->version(), "1.2.3");
}

template <typename P>
void PdCom5TestBase<P>::_implParameterEventSubscription()
{
    client_->find("/dir1/param01");
    ASSERT_TRUE(client_->readUntil(
            [&c = *client_] { return !c.found_vars_.empty(); }));
    ASSERT_EQ(client_->found_vars_.size(), 1) << "One variable has been found";
    const auto var = client_->found_vars_.back();
    ASSERT_FALSE(var.empty()) << "Variable must not be empty";
    EXPECT_EQ(var.getPath(), "/dir1/param01");
    EXPECT_TRUE(var.getSizeInfo().isScalar());
    EXPECT_EQ(var.getTypeInfo().element_size, sizeof(int));
    MySubscriber ms(PdCom::event_mode);
    PdCom::Subscription sub(ms, var);
    ASSERT_TRUE(client_->readUntil([&ms] {
        return !ms.recieved_states_.empty()
                && ms.recieved_states_.back()
                == PdCom::Subscription::State::Active;
    }));
    if (!ms.recieved_newvalues_.empty()) {
        int i = 909090;
        sub.getValue(i);
        EXPECT_EQ(i, 0) << "Parameter has not been written yet";
        ms.recieved_newvalues_.clear();
    }
    const int test_data = 47811;
    EXPECT_TRUE(client_->synchronize(var.setValue(test_data)))
            << "Variable is writeable";
    ASSERT_TRUE(client_->readUntil(
            [&ms] { return !ms.recieved_newvalues_.empty(); }));
    {
        int i = 808080;
        sub.getValue(i);
        EXPECT_EQ(i, test_data)
                << "Parameter update notification arrived correctly";
    }
    EXPECT_EQ(server_->getparam1(), test_data)
            << "Parameter was written correctly";
}

TEST_F(PdCom5CppTests, ParameterEventSubscription)
{
    PdCom5CppTests::_implParameterEventSubscription();
}


TEST_F(PdCom5CppTlsTests, ParameterEventSubscription)
{
    PdCom5CppTlsTests::_implParameterEventSubscription();
}

template <typename P>
void PdCom5TestBase<P>::_implSplitVectorParameterEventSubscription()
{
    client_->find("/dir1/param02/2");
    ASSERT_TRUE(client_->readUntil(
            [&c = *client_] { return !c.found_vars_.empty(); }));
    ASSERT_EQ(client_->found_vars_.size(), 1) << "One variable has been found";
    const auto scalar_var = client_->found_vars_.back();
    ASSERT_FALSE(scalar_var.empty()) << "Variable must not be empty";
    EXPECT_EQ(scalar_var.getPath(), "/dir1/param02/2");
    EXPECT_TRUE(scalar_var.getSizeInfo().isScalar());
    EXPECT_EQ(scalar_var.getTypeInfo().element_size, sizeof(int));
    client_->found_vars_.clear();
    client_->find("/dir1/param02");
    ASSERT_TRUE(client_->readUntil(
            [&c = *client_] { return !c.found_vars_.empty(); }));
    ASSERT_EQ(client_->found_vars_.size(), 1) << "One variable has been found";
    const auto vector_var = client_->found_vars_.back();
    ASSERT_FALSE(vector_var.empty()) << "Variable must not be empty";
    EXPECT_TRUE(vector_var.getSizeInfo().isVector());
    EXPECT_EQ(vector_var.getTypeInfo().element_size, sizeof(int));

    MySubscriber ms(PdCom::event_mode);
    PdCom::Subscription sub(ms, scalar_var);
    ASSERT_TRUE(client_->readUntil([&ms] {
        return !ms.recieved_states_.empty()
                && ms.recieved_states_.back()
                == PdCom::Subscription::State::Active;
    }));
    if (!ms.recieved_newvalues_.empty()) {
        int i = 909090;
        sub.getValue(i);
        EXPECT_EQ(i, 0) << "Parameter has not been written yet";
        ms.recieved_newvalues_.clear();
    }
    const int test_data = 47811;
    EXPECT_TRUE(client_->synchronize(scalar_var.setValue(test_data)))
            << "Scalar Variable is writeable";
    ASSERT_TRUE(client_->readUntil([&ms] {
        return !ms.recieved_newvalues_.empty();
    })) << "setValue() on scalar variable results in notification";
    {
        int i = 808080;
        sub.getValue(i);
        EXPECT_EQ(i, test_data)
                << "Parameter update notification arrived correctly";
        ms.recieved_newvalues_.clear();
    }
    EXPECT_EQ(server_->getparam2().at(2), test_data)
            << "Parameter was written correctly";

    const TestServer::param2_type expected2 = {54, 74, 90, 2010};
    EXPECT_TRUE(client_->synchronize(vector_var.setValue(expected2)))
            << "Vector Variable is writeable";
    ASSERT_TRUE(client_->readUntil([&ms] {
        return !ms.recieved_newvalues_.empty();
    })) << "setValue() on vector variable results in notification of scalar";
    {
        int i = 808080;
        sub.getValue(i);
        EXPECT_EQ(i, expected2.at(2))
                << "Parameter update notification arrived correctly";
        ms.recieved_newvalues_.clear();
    }
}

TEST_F(PdCom5CppTests, SplitVectorParameterEventSubscription)
{
    PdCom5CppTests::_implSplitVectorParameterEventSubscription();
}


TEST_F(PdCom5CppTlsTests, SplitVectorParameterEventSubscription)
{
    PdCom5CppTlsTests::_implSplitVectorParameterEventSubscription();
}


template <typename P>
void PdCom5TestBase<P>::_implSplitVectorSignalEventSubscription()
{
    client_->find("/dir1/signal2/3");
    ASSERT_TRUE(client_->readUntil(
            [&c = *client_] { return !c.found_vars_.empty(); }));
    ASSERT_EQ(client_->found_vars_.size(), 1) << "One variable has been found";
    const auto scalar_var = client_->found_vars_.back();
    ASSERT_FALSE(scalar_var.empty()) << "Variable must not be empty";
    EXPECT_TRUE(scalar_var.getSizeInfo().isScalar());
    EXPECT_EQ(scalar_var.getTypeInfo().element_size, sizeof(short));
    client_->found_vars_.clear();

    MySubscriber ms(PdCom::event_mode);
    PdCom::Subscription sub(ms, scalar_var);
    ASSERT_TRUE(client_->readUntil([&ms] {
        return !ms.recieved_states_.empty()
                && ms.recieved_states_.back()
                == PdCom::Subscription::State::Active;
    }));
    sub.poll();
    ASSERT_TRUE(client_->readUntil([&ms] {
        return !ms.recieved_newvalues_.empty();
    })) << "poll() arrived";
    {
        int i = 808080;
        sub.getValue(i);
        EXPECT_EQ(i, 0) << "poll initial zero";
        ms.recieved_newvalues_.clear();
    }
    const TestServer::signal2_type expected1 = {4, 7, 1, 1};
    server_->setsignal2(expected1);
    ASSERT_TRUE(client_->readUntil([&ms] {
        return !ms.recieved_newvalues_.empty();
    })) << "signal update arrived";
    {
        int i = 808080;
        sub.getValue(i);
        EXPECT_EQ(i, expected1[3]);
        ms.recieved_newvalues_.clear();
    }
}

TEST_F(PdCom5CppTests, SplitVectorSignalEventSubscription)
{
    PdCom5CppTests::_implSplitVectorSignalEventSubscription();
}


TEST_F(PdCom5CppTlsTests, SplitVectorSignalEventSubscription)
{
    PdCom5CppTlsTests::_implSplitVectorSignalEventSubscription();
}

::testing::AssertionResult
compare(const param03_type::Value &val1, const param03_type::Value &val2)
{
    for (unsigned int row = 0; row < param03_type::Value::Rows; ++row)
        for (unsigned col = 0; col < param03_type::Value::Cols; ++col)
            if (val1.value_[row][col] != val2.value_[row][col])
                return ::testing::AssertionFailure()
                        << "Value mismatch at Row " << row << " and col "
                        << col;

    return ::testing::AssertionSuccess();
}

template <typename P>
void PdCom5TestBase<P>::_implMultidimParameter()
{
    client_->find("/dir1/param03");
    ASSERT_TRUE(client_->readUntil(
            [&c = *client_] { return !c.found_vars_.empty(); }));
    ASSERT_EQ(client_->found_vars_.size(), 1) << "One variable has been found";
    const PdCom::Variable var = client_->found_vars_.back();
    ASSERT_FALSE(var.empty()) << "Variable must not be empty";
    const auto si = var.getSizeInfo();
    ASSERT_TRUE(si.is2DMatrix()) << "Variable is 2D Matrix";
    ASSERT_EQ(si.columns(), param03_type::Value::Cols) << "Column mismatch";
    ASSERT_EQ(si.rows(), param03_type::Value::Rows) << "Row mismatch";

    const PdCom::ScalarSelector sel1 {{0, 2}},  // first row, last col
            sel2 {{1, 0}}                       // second row, first col
    ;

    MySubscriber ms(PdCom::event_mode);
    PdCom::Subscription sub1(ms, var);
    ASSERT_TRUE(client_->readUntil([&ms] {
        return !ms.recieved_states_.empty()
                && ms.recieved_states_.back()
                == PdCom::Subscription::State::Active;
    }));
    ms.recieved_states_.clear();
    PdCom::Subscription sub2(ms, var, sel1);
    ASSERT_TRUE(client_->readUntil([&ms] {
        return !ms.recieved_states_.empty()
                && ms.recieved_states_.back()
                == PdCom::Subscription::State::Active;
    }));

    const auto check = [&](int run, const param03_type::Value &expected) {
        {
            const auto actual1 = server_->getparam3();
            EXPECT_TRUE(compare(expected, actual1))
                    << "setValue value mismatch at run " << run;
        }
        {
            param03_type::Value actual2 = {};
            sub1.getValue(actual2.value_);
            EXPECT_TRUE(compare(expected, actual2))
                    << "getValue() value mismatch at run " << run;
        }
        {
            param03_type::Value::value_type val = {};
            sub2.getValue(val);
            EXPECT_EQ(val, expected.value_[0][2])
                    << "getValue() w/ ScalarSelector mismatch at run " << run;
        }
    };


    const param03_type::Value expected1 = {{{1, 2, 3}, {11, 12, 13}}};
    const param03_type::Value expected2 = {{{1, 2, 3}, {4711, 12, 13}}};

    ASSERT_TRUE(client_->synchronize(var.setValue(expected1.value_)));
    ASSERT_TRUE(client_->readUntil(
            [&ms] { return !ms.recieved_newvalues_.empty(); }));
    ms.recieved_newvalues_.clear();
    check(1, expected1);

    ASSERT_TRUE(
            client_->synchronize(var.setValue(expected2.value_[1][0], sel2)))
            << "setValue() w/ selector";
    ASSERT_TRUE(client_->readUntil(
            [&ms] { return !ms.recieved_newvalues_.empty(); }));
    ms.recieved_newvalues_.clear();
    check(2, expected2);
}

TEST_F(PdCom5CppTests, MultidimParameter)
{
    PdCom5CppTests::_implMultidimParameter();
}


TEST_F(PdCom5CppTlsTests, MultidimParameter)
{
    PdCom5CppTlsTests::_implMultidimParameter();
}

template <typename P>
void PdCom5TestBase<P>::_implSignalPeriodicSubscription()
{
    client_->find("/dir1/signal1");
    ASSERT_TRUE(client_->readUntil(
            [&c = *client_] { return !c.found_vars_.empty(); }));
    ASSERT_EQ(client_->found_vars_.size(), 1) << "One variable has been found";
    const auto var = client_->found_vars_.back();
    ASSERT_FALSE(var.empty()) << "Variable must not be empty";
    EXPECT_EQ(var.getPath(), "/dir1/signal1");
    EXPECT_TRUE(var.getSizeInfo().isScalar());
    EXPECT_EQ(var.getTypeInfo().element_size, sizeof(double));
    PdCom::Subscription sub1;
    constexpr int decimation = 10;
    constexpr auto period    = TestServer::task1_period * decimation;
    MyDoubleSubscriber subscriber(period, sub1);
    sub1 = {subscriber, var};
    ASSERT_NE(sub1.getState(), PdCom::Subscription::State::Invalid)
            << "Subscription has left invalid state";
    ASSERT_TRUE(client_->readUntil([&subscriber] {
        return !subscriber.recieved_states_.empty()
                && subscriber.recieved_states_.back()
                == PdCom::Subscription::State::Active;
    }));
    ASSERT_TRUE(client_->readUntil(
            [&subscriber] {
                return subscriber.recieved_newvalues_.size() >= 5;
            },
            10));
    sub1 = {};
    for (unsigned int i = 0; i < subscriber.recieved_newvalues_.size() - 1;
         i += 2) {
        EXPECT_NEAR(
                subscriber.recieved_newvalues_[i].first.count(),
                (subscriber.recieved_newvalues_[i + 1].first - period).count(),
                std::chrono::nanoseconds {TestServer::task1_period}.count() / 2)
                << "Timestamp mismatch";
        EXPECT_EQ(
                subscriber.recieved_newvalues_[i].second,
                subscriber.recieved_newvalues_[i + 1].second - decimation)
                << "Value mismatch";
    }
    SUCCEED() << "Value offset is "
              << subscriber.recieved_newvalues_.front().second;
}

TEST_F(PdCom5CppTests, SignalPeriodicSubscription)
{
    PdCom5CppTests::_implSignalPeriodicSubscription();
}

TEST_F(PdCom5CppTlsTests, SignalPeriodicSubscription)
{
    PdCom5CppTlsTests::_implSignalPeriodicSubscription();
}

template <typename P>
template <typename SecondProcess>
void PdCom5TestBase<P>::_implParameterUpdateViaSecondConnection()
{
    server_->setparam1(4711);
    MySubscriber ms(PdCom::event_mode);
    PdCom::Subscription sub(ms, *client_, "/dir1/param01");
    ASSERT_TRUE(client_->readUntil([&ms] {
        return !ms.recieved_states_.empty()
                && ms.recieved_states_.back()
                == PdCom::Subscription::State::Active;
    }));
    const auto initial_newvalues = ms.recieved_newvalues_.size();
    SecondProcess client2("localhost");
    client2.connect();
    EXPECT_FALSE(client2.find("/dir1/param01"));
    client2.readUntil([&client2] { return !client2.found_vars_.empty(); });
    ASSERT_EQ(client2.found_vars_.size(), 1) << "One variable has been found";
    const auto var2 = client2.found_vars_.back();
    ASSERT_FALSE(var2.empty()) << "Variable must not be empty";
    const int next_value = 2468;
    ASSERT_TRUE(client_->synchronize(var2.setValue(next_value)))
            << "Setting parameter from second connection";
    ASSERT_TRUE(client_->readUntil([&ms, initial_newvalues] {
        return ms.recieved_newvalues_.size() > initial_newvalues;
    }));
    int read_value = 0;
    sub.getValue(read_value);
    EXPECT_EQ(read_value, next_value)
            << "First connection got correct parameter";
    EXPECT_EQ(server_->getparam1(), next_value)
            << "RT process got correct parameter";
    ASSERT_TRUE(client2.synchronize(
            var2.poll(), [&next_value](auto value, auto /* ts */) {
                int val;
                value.getValue(val);
                if (next_value != val)
                    return ::testing::AssertionFailure()
                            << "client2 poll failed";
                return ::testing::AssertionSuccess();
            }));
}

TEST_F(PdCom5CppTests, UpdateParameterSecondConnection)
{
    _implParameterUpdateViaSecondConnection<MyProcess>();
}

TEST_F(PdCom5CppTlsTests, UpdateParameterSecondConnection)
{
    _implParameterUpdateViaSecondConnection<MyProcess>();
}

TEST_F(PdCom5CppTlsTests, UpdateParameterSecondConnectionTls)
{
    _implParameterUpdateViaSecondConnection<MyTlsProcess>();
}


template <typename P>
void PdCom5TestBase<P>::_implCheckBroadcast(
        const std::string &message,
        const char *attribute,
        const char *user,
        int max_td)
{
    struct clearer
    {
        std::vector<MyBroadcast> &b_;
        clearer(std::vector<MyBroadcast> &b) : b_(b) {}
        ~clearer() { b_.clear(); }
    } const clearer(client_->broadcasts_);
    client_->broadcast(message, attribute);
    ASSERT_TRUE(client_->readUntil(
            [&b = client_->broadcasts_] { return b.size() == 1; }));
    EXPECT_EQ(client_->broadcasts_.back().message_, message);
    EXPECT_EQ(client_->broadcasts_.back().attribute_, attribute);
    EXPECT_EQ(client_->broadcasts_.back().user_, user);
    ::timespec t;
    ASSERT_EQ((clock_gettime(CLOCK_MONOTONIC, &t)), 0);
    EXPECT_LT(
            std::abs(
                    t.tv_sec
                    - static_cast<time_t>(
                            std::chrono::duration_cast<std::chrono::seconds>(
                                    client_->broadcasts_.back().time_ns_)
                                    .count())),
            max_td)
            << "Time delta less than " << max_td << " seconds";
}

TEST_F(PdCom5CppTests, Broadcast)
{
    _implCheckBroadcast("My'Message\"With&Special<chars>", "text");
    _implCheckBroadcast("My'Message\"With&Special<chars>", "action");
    std::string s {"My Message"};
    s[2] = '\0';
    EXPECT_THROW(_implCheckBroadcast(s, "text"), PdCom::InvalidArgument);
}

TEST_F(PdCom5CppTlsTests, Broadcast)
{
    _implCheckBroadcast("My'Message\"With&Special<chars>", "text");
    _implCheckBroadcast("My'Message\"With&Special<chars>", "action");
    std::string s {"My Message"};
    s[2] = '\0';
    EXPECT_THROW(_implCheckBroadcast(s, "text"), PdCom::InvalidArgument);
}

TEST_F(PdCom5CppTests, getUnknownMessage)
{
    MyMessageManager mm;
    client_->setMessageManager(&mm);
    mm.getMessage(4711);
    ASSERT_TRUE(client_->readUntil(
            [&mm] { return !mm.get_message_reply_.empty(); }));
    ASSERT_EQ(mm.get_message_reply_.size(), 1) << "one message was returned";
    EXPECT_EQ(mm.get_message_reply_.front().seqNo, 4711)
            << "number is identical";
    EXPECT_TRUE(mm.get_message_reply_.front().path.empty())
            << "path of unknown message is empty";
}

::testing::AssertionResult
compare_message(const PdCom::Message &expected, const PdCom::Message &actual)
{
    const std::chrono::nanoseconds max_delta {std::chrono::seconds {2}};

    if (expected.level != actual.level)
        return ::testing::AssertionFailure()
                << "Log level mismatch, got " << static_cast<int>(actual.level)
                << ", expected " << static_cast<int>(expected.level);

    if (expected.path != actual.path)
        return ::testing::AssertionFailure()
                << "Path mismatch, got " << actual.path << ", expected "
                << expected.path;

    if (expected.level != PdCom::LogLevel::Reset
        && expected.text != actual.text)
        return ::testing::AssertionFailure()
                << "Text mismatch, got " << actual.text << ", expected "
                << expected.text;

    const auto diff = actual.time - expected.time;
    if (std::abs(diff.count()) > max_delta.count())
        return ::testing::AssertionFailure()
                << "Timestamp difference larger than 2s";

    if (expected.index != actual.index)
        return ::testing::AssertionFailure()
                << "Index mismatch, got " << actual.index << ", expected "
                << expected.index;

    return ::testing::AssertionSuccess();
}

TEST_F(PdCom5CppTests, getUnsolicitedVectorMessages)
{
    MyMessageManager mm;
    client_->setMessageManager(&mm);
    const auto now                = TestServer::get_time();
    server_->event_control_.at(0) = CRIT_EVENT;
    server_->event_control_.at(1) = WARN_EVENT;
    // wait for server to ack
    while (server_->event_control_.at(0) != -1) {
        usleep(1000);
    }
    server_->event_control_.at(0) = RESET_EVENT;
    while (server_->event_control_.at(0) != -1) {
        usleep(1000);
    }
    ASSERT_TRUE(client_->readUntil(
            [&mm] { return mm.process_message_.size() == 3; }));
    EXPECT_TRUE(mm.get_message_reply_.empty()) << "no getMessage() reply";
    EXPECT_TRUE(mm.active_messages_reply_.empty()) << "no active message reply";
    const PdCom::Message expected_messages[3] = {
            {0, PdCom::LogLevel::Critical, Event1Messages::path, now,
             Event1Messages::messages[0], 0},
            {0, PdCom::LogLevel::Warn, Event1Messages::path, now,
             Event1Messages::messages[1], 1},
            {0, PdCom::LogLevel::Reset, Event1Messages::path, now, "", 0},
    };

    for (unsigned int i = 0; i < 3; ++i) {
        EXPECT_TRUE(compare_message(
                expected_messages[i], mm.process_message_.at(i)))
                << "Message " << i << " is as expected";
    }
}


TEST_F(PdCom5CppTests, getUnsolicitedMessage)
{
    MyMessageManager mm;
    client_->setMessageManager(&mm);
    const auto now                 = TestServer::get_time();
    server_->single_event_control_ = CRIT_EVENT;
    // wait for server to ack
    while (server_->single_event_control_ != -1) {
        usleep(1000);
    }
    ASSERT_TRUE(client_->readUntil(
            [&mm] { return mm.process_message_.size() == 1; }));
    server_->single_event_control_ = RESET_EVENT;
    while (server_->single_event_control_ != -1) {
        usleep(1000);
    }
    ASSERT_TRUE(client_->readUntil(
            [&mm] { return mm.process_message_.size() == 2; }));
    EXPECT_TRUE(mm.get_message_reply_.empty()) << "no getMessage() reply";
    EXPECT_TRUE(mm.active_messages_reply_.empty()) << "no active message reply";
    const PdCom::Message expected_messages[2] = {
            {0, PdCom::LogLevel::Critical, Event2Path, now, Event2Text, -1},
            {0, PdCom::LogLevel::Reset, Event2Path, now, "", -1},
    };

    for (unsigned int i = 0; i < 2; ++i) {
        EXPECT_TRUE(compare_message(
                expected_messages[i], mm.process_message_.at(i)))
                << "Message " << i << " is as expected";
    }

    const auto seqNo = mm.process_message_.at(0).seqNo;
    mm.process_message_.clear();
    mm.get_message_reply_.clear();
    mm.getMessage(seqNo);
    ASSERT_TRUE(client_->readUntil([&mm] {
        return mm.get_message_reply_.size() == 1;
    })) << "fetch first message";
    EXPECT_TRUE(compare_message(
            expected_messages[0], mm.get_message_reply_.front()));
}

TEST_F(PdCom5CppTests, getActiveMessages)
{
    client_.reset();
    const auto now                = TestServer::get_time();
    server_->event_control_.at(0) = CRIT_EVENT;
    server_->event_control_.at(1) = WARN_EVENT;
    // wait for server to ack
    while (server_->event_control_.at(0) != -1) {
        usleep(1000);
    }
    server_->event_control_.at(0) = RESET_EVENT;
    while (server_->event_control_.at(0) != -1) {
        usleep(1000);
    }

    // a delay seems to be necessary to let the messages propagate to the nrt
    // process
    usleep(100000);

    MyMessageManager mm;
    client_ = std::make_unique<MyProcess>("localhost");
    client_->connect();
    client_->setMessageManager(&mm);
    mm.activeMessages();
    ASSERT_TRUE(client_->readUntil(
            [&mm] { return mm.active_messages_reply_.size() == 1; }));
    EXPECT_TRUE(mm.get_message_reply_.empty()) << "no getMessage() Reply";
    EXPECT_TRUE(mm.process_message_.empty()) << "no unsolicited messages";
    ASSERT_EQ(mm.active_messages_reply_.front().size(), 2)
            << "one reply with two messages";
    const PdCom::Message expected_messages[2] = {
            {0, PdCom::LogLevel::Warn, Event1Messages::path, now,
             Event1Messages::messages[1], 1},
            {0, PdCom::LogLevel::Reset, Event1Messages::path, now, "", 0},
    };

    for (unsigned int i = 0; i < 2; ++i) {
        EXPECT_TRUE(compare_message(
                expected_messages[i], mm.active_messages_reply_.front().at(i)))
                << "Message " << i << " is as expected";
    }

    // get the first message
    const auto prev_seq = mm.active_messages_reply_.front().at(0).seqNo - 1;
    mm.getMessage(prev_seq);
    mm.active_messages_reply_.clear();
    mm.process_message_.clear();
    ASSERT_TRUE(client_->readUntil(
            [&mm] { return mm.get_message_reply_.size() == 1; }));
    EXPECT_TRUE(mm.active_messages_reply_.empty()) << "no active message reply";
    EXPECT_TRUE(mm.process_message_.empty()) << "no unsolicited messages";

    const PdCom::Message expected {
            0,   PdCom::LogLevel::Critical,   Event1Messages::path,
            now, Event1Messages::messages[0], 0};

    EXPECT_EQ(mm.get_message_reply_.front().seqNo, prev_seq);
    EXPECT_TRUE(compare_message(expected, mm.get_message_reply_.front()))
            << "getMessage() got it right";
}

TEST_F(PdCom5CppTests, ListAllVariables)
{
    EXPECT_FALSE(client_->list("")) << "No cached variables at begin";
    ASSERT_TRUE(client_->readUntil(
            [this] { return !client_->list_replies_.empty(); }));
    ASSERT_EQ(client_->list_replies_.size(), 1) << "Exactly one reply";
    EXPECT_TRUE(client_->list_replies_[0].dirs.empty())
            << "No dirs when list(\"\")";
    const auto vars = std::move(client_->list_replies_[0].vars);

    const char *const expected_vars[] = {
            "/dir1/signal1",
            "/dir1/signal2",
            "/dir1/param01",
            "/dir1/param02",
            "/dir1/param03",
            "/operation/hour/counter/running",
            "/operation/hour/counter/restore",
    };
    for (const auto expected : expected_vars) {
        const auto it = std::find_if(
                vars.begin(), vars.end(),
                [expected](const PdCom::Variable &var) {
                    return var.getPath() == expected;
                });
        EXPECT_NE(it, vars.end()) << "Variable" << expected << "found";
    }
}

TEST_F(PdCom5CppTests, ListDirectories)
{
    EXPECT_FALSE(client_->list("/operation")) << "No cached variables at begin";
    ASSERT_TRUE(client_->readUntil(
            [this] { return !client_->list_replies_.empty(); }));
    ASSERT_EQ(client_->list_replies_.size(), 1) << "Exactly one reply";

    EXPECT_TRUE(client_->list_replies_[0].vars.empty())
            << "Dir contains no variables";
    ASSERT_EQ(client_->list_replies_[0].dirs.size(), 1)
            << "Dir contains one directory";
    EXPECT_EQ(client_->list_replies_[0].dirs[0], "/operation/hour");
}

TEST_F(PdCom5CppTests, ListVarsDirectories)
{
    EXPECT_FALSE(client_->list("/operation/hour/counter"))
            << "No cached variables at begin";
    ASSERT_TRUE(client_->readUntil(
            [this] { return !client_->list_replies_.empty(); }));
    ASSERT_EQ(client_->list_replies_.size(), 1) << "Exactly one reply";

    EXPECT_TRUE(client_->list_replies_[0].dirs.empty())
            << "Dir contains no dirs";
    ASSERT_EQ(client_->list_replies_[0].vars.size(), 2)
            << "Dir contains two vars";


    const auto vars = std::move(client_->list_replies_[0].vars);
    const char *const expected_vars[] = {
            "/operation/hour/counter/running",
            "/operation/hour/counter/restore",
    };

    for (const auto expected : expected_vars) {
        const auto it = std::find_if(
                vars.begin(), vars.end(),
                [expected](const PdCom::Variable &var) {
                    return var.getPath() == expected;
                });
        EXPECT_NE(it, vars.end()) << "Variable" << expected << "found";
    }
}

TEST(PdCom5CppTlsClientAuthReject, RejectSelfSigned)
{
    struct TlsInit
    {
        TlsInit() { PdCom::SecureProcess::InitLibrary(); }
        ~TlsInit() { PdCom::SecureProcess::FinalizeLibrary(); }
    } const tlsinit;

    const auto server_ = std::make_unique<TestServer>();
    server_->set_config_file_path(
            std::string(config_dir) + "/pdserv.tls-clientauth.conf");

    server_->prepare();
    server_->start();
    const auto client_ = std::make_unique<MyTlsClientAuthProcess>(
            "localhost", "/noca_client");
    // gnutls_handshake() returns 0, howewer
    // EXPECT_THROW(client_ = connect(), PdCom::PosixProcess::ReadFailure);
    ASSERT_TRUE(client_);
    ASSERT_TRUE(client_->readUntil(
            [&client_] { return client_->connected_ or client_->isEof(); }));
    EXPECT_TRUE(client_->isEof()) << "Server has closed the socket";
    EXPECT_FALSE(client_->connected_);
}

TEST_F(PdCom5SaslTests, LoginUnknownUser)
{
    MyLoginManager lm("localhost");
    client_->setAuthManager(&lm);
    EXPECT_TRUE((client_->login_unknown_user(lm, false)));
    EXPECT_FALSE(client_->connected_) << "Client is still not connected";
}

TEST_F(PdCom5SaslTests, LoginDefaultRealm)
{
    MyLoginManager lm("localhost");
    client_->setAuthManager(&lm);
    EXPECT_TRUE(client_->do_login(
            lm, MyLoginManager::NextAction::Ernie,
            MyLoginManager::NextAction::Ernie, LoginResult::Success, false))
            << "Login works with default realm";
    client_->asyncData();
    client_->connect();
    EXPECT_TRUE(client_->connected_);
}

TEST_F(PdCom5SaslTests, LoginWrongPassword)
{
    MyLoginManager lm("localhost");
    client_->setAuthManager(&lm);
    EXPECT_TRUE(client_->login_wrong_password(lm, false));
    EXPECT_FALSE(client_->connected_) << "Client is still not connected";
}

TEST_F(PdCom5SaslTests, CancelLogin)
{
    MyLoginManager lm("localhost");
    client_->setAuthManager(&lm);
    lm.next_action_.push(MyLoginManager::NextAction::Cancel);
    ASSERT_TRUE(client_->readUntil([&lm] { return !lm.completed_.empty(); }));
    EXPECT_EQ(LoginResult::Canceled, lm.completed_.back())
            << "Login was canceled";
    EXPECT_FALSE(client_->connected_) << "Client is still not connected";
    EXPECT_TRUE(client_->login_ok(lm));

    client_.reset();
    EXPECT_THROW(lm.login(), PdCom::ProcessGoneAway)
            << "No Process use after free";
}

TEST_F(PdCom5SaslTests, NoAuthManager)
{
    EXPECT_THROW(client_->connect(), PdCom::LoginRequired);
}

TEST_F(PdCom5SaslTests, DISABLED_ChangeAuthManager)
{
    MyLoginManager lm("localhost");
    client_->setAuthManager(&lm);
    ASSERT_TRUE(client_->login_ok(lm, false));
    client_->connect();
    lm.logout();
    {
        MyLoginManager lm2("localhost");
        client_->setAuthManager(&lm2);
        EXPECT_THROW(lm.login(), PdCom::ProcessGoneAway)
                << "Login is no longer possible because AuthManager is "
                   "replaced";

        ASSERT_TRUE(client_->login_ok(lm2));
        client_->reset();
        EXPECT_THROW(lm2.logout(), PdCom::NotConnected) << "No use after free";
    }
}

TEST_F(PdCom5SaslTests, DISABLED_MoveConstructedAuthManager)
{
    MyLoginManager lm("localhost");
    client_->setAuthManager(&lm);
    ASSERT_TRUE(client_->login_ok(lm, false));
    client_->connect();
    lm.logout();
    {
        MyLoginManager lm2 {std::move(lm)};
        EXPECT_THROW(lm.login(), PdCom::Exception)
                << "Login is no longer possible because AuthManager is "
                   "moved from";
        EXPECT_THROW(lm.logout(), PdCom::Exception)
                << "Logout is no longer possible because AuthManager is "
                   "moved from";

        client_->setAuthManager(&lm2);
        ASSERT_TRUE(client_->login_ok(lm2));
        lm2.logout();
    }
}

TEST_F(PdCom5SaslTests, DISABLED_MoveAssignedAuthManager)
{
    struct NullProcess : PdCom::Process
    {
        int read(char *, int) override { return -1; }
        void write(const char *, size_t) override {}
        void flush() override {}
        void connected() override {}

        using Process::Process;
        using Process::setAuthManager;
    } p2;
    MyLoginManager lm("localhost");
    client_->setAuthManager(&lm);
    ASSERT_TRUE(client_->login_ok(lm, false));
    client_->connect();
    lm.logout();
    {
        MyLoginManager lm2 {"localhost"};
        p2.setAuthManager(&lm2);
        lm2 = std::move(lm);

        EXPECT_THROW(lm.login(), PdCom::Exception)
                << "Login is no longer possible because AuthManager is "
                   "moved from";
        EXPECT_THROW(lm.logout(), PdCom::Exception)
                << "Logout is no longer possible because AuthManager is "
                   "moved from";

        ASSERT_TRUE(client_->login_ok(lm2))
                << "AM is now assigned to Process 1";
        lm2.logout();
    }
}


TEST(PdCom5PersistentTests, PersistentParameter)
{
    const char *const parameter = "/dir1/param01";
    const int param1_value      = 4711;
    {
        TestServer server;
        server.set_config_file_path(
                std::string(config_dir) + "/pdserv.persistent.conf");
        ASSERT_NO_THROW(server.prepare());
        server.start();
        MyProcess client("localhost");
        client.connect();
        client.find(parameter);
        client.readUntil([&client] { return !client.found_vars_.empty(); });
        ASSERT_FALSE(client.found_vars_.back().empty()) << "Parameter found";
        ASSERT_TRUE(client.synchronize(
                client.found_vars_.back().setValue(param1_value)));
        bool param_updated = false;
        for (int i = 0; i < 100; ++i) {
            if (server.getparam1() == param1_value) {
                param_updated = true;
                break;
            }
            usleep(1000);
        }
        ASSERT_TRUE(param_updated) << "RT Process got parameter update";
    }
    {
        TestServer server;
        server.set_config_file_path(
                std::string(config_dir) + "/pdserv.persistent.conf");
        ASSERT_NO_THROW(server.prepare());
        server.start();
        MyProcess client("localhost");
        client.connect();
        MySubscriber ms(PdCom::poll_mode);
        PdCom::Subscription sub1(ms, client, parameter);
        client.readUntil([&sub1] {
            return sub1.getState() == PdCom::Subscription::State::Active;
        });
        sub1.poll();
        client.readUntil([&ms] { return !ms.recieved_newvalues_.empty(); });
        int v = 111111;
        sub1.getValue(v);
        ASSERT_EQ(v, param1_value) << "Parameter value is persistent";
    }
}

TEST(PortInUse, PortInUse)
{
    TestServer server1;
    ASSERT_NO_THROW(server1.prepare());
    server1.start();

    TestServer server2;
    EXPECT_EQ(server2.tryPrepare(), -EADDRINUSE)
            << "Server could not be started";
}

TEST(PortInUse, TlsPortInUse)
{
    TestServer server1;
    server1.set_config_file_path(
            std::string(config_dir) + "/pdserv.tls-simple.conf");
    ASSERT_NO_THROW(server1.prepare());
    server1.start();

    TestServer server2;
    server2.set_config_file_path(
            std::string(config_dir) + "/pdserv.tls-simple-altport.conf");
    EXPECT_EQ(server2.tryPrepare(), -EADDRINUSE)
            << "Server could not be started";
}

TEST(PdCom5PersistentTests, ParameterSignalPairPdCom)
{
    const unsigned operating_hours = 1995;

    const auto execute = [&](bool set_value, const char *check_var,
                             const char *msg) {
        TestServer server;
        server.set_config_file_path(
                std::string(config_dir) + "/pdserv.persistent-pair.conf");
        try {
            server.prepare();
        }
        catch (...) {
            return ::testing::AssertionFailure() << "prepare threw exception";
        }
        server.start();
        if (set_value) {
            server.settime_counter_running(operating_hours);
            // let value propagate to pdserv persistent system
            sleep(1);
        }
        MyProcess client("localhost");
        client.connect();
        MySubscriber ms(PdCom::poll_mode);
        PdCom::Subscription sub1(ms, client, check_var);
        client.readUntil([&sub1] {
            return sub1.getState() == PdCom::Subscription::State::Active;
        });
        sub1.poll();
        client.readUntil([&ms] { return !ms.recieved_newvalues_.empty(); });
        unsigned v = 111111;
        sub1.getValue(v);
        if (v != operating_hours)
            return ::testing::AssertionFailure() << msg;
        return ::testing::AssertionSuccess();
    };

    ASSERT_TRUE(
            execute(true, "/operation/hour/counter/running",
                    "Value has arrived in pdserv"))
            << "Initializing operation time counter";
    ASSERT_TRUE(
            execute(false, "/operation/hour/counter/restore",
                    "Value was restored from database"))
            << "Restoring operation time counter";
}

TEST(PdCom5PersistentTests, ParameterSignalPairRaw)
{
    const unsigned operating_hours = 1995;
    {
        TestServer server;
        server.set_config_file_path(
                std::string(config_dir) + "/pdserv.persistent-pair.conf");
        ASSERT_NO_THROW(server.prepare());
        server.settime_counter_running(operating_hours);
        server.start();
        sleep(1);
        server.stop();
    }

    {
        TestServer server;
        server.set_config_file_path(
                std::string(config_dir) + "/pdserv.persistent-pair.conf");
        ASSERT_NO_THROW(server.prepare());
        EXPECT_EQ(server.gettime_counter_restore(), operating_hours)
                << "Signal was restored";
        server.start();
        sleep(1);
        server.stop();
    }
}
