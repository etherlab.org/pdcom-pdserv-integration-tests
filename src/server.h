/*****************************************************************************
 *
 * Copyright (C) 2021 Bjarne von Horn (vh at igh dot de)
 *
 * This file is part of the PdCom library.
 *
 * The PdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The PdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the PdCom library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

#pragma once

#include "pdservpp.h"

#include <array>
#include <atomic>
#include <chrono>
#include <memory>
#include <mutex>
#include <pdserv.h>
#include <thread>

#define DECLARE_SET_GET(dtype, name) \
  public: \
    dtype get##name() const \
    { \
        std::lock_guard<std::mutex> lck(mut_); \
        return name##_.val_; \
    } \
    void set##name(dtype val) \
    { \
        std::lock_guard<std::mutex> lck(mut_); \
        name##_.val_ = val; \
    }

#define DECLARE_SIGNAL(dtype, name, task, decimation, path) \
  private: \
    struct name##wrapper \
    { \
        dtype val_ = {}; \
        name##wrapper(PdServ::Task &t) { t.signal(decimation, path, val_); } \
    } name##_ = {task}; \
\
    DECLARE_SET_GET(dtype, name)

#define DECLARE_PARAMETER(dtype, name, mode, path) \
  private: \
    struct name##wrapper \
    { \
        dtype val_ = {}; \
        name##wrapper(PdServ::ServBase &s) { s.parameter(path, mode, val_); } \
    } name##_ = {server_}; \
\
    DECLARE_SET_GET(dtype, name)

struct Event1Messages
{
    static constexpr const char *messages[] = {
            "First Index",
            "Second Index",
            "Third Index",
    };

    static constexpr const char path[] = "/Event1";
};
using Event1Type = PdServ::MultiDimensionEvent<Event1Messages>;

constexpr const char *Event2Path = "/Event2";
constexpr const char *Event2Text = "Single Event";

template <size_t rows, size_t cols>
struct param2d_wrapper
{
    struct Value
    {
        static constexpr size_t Rows = rows, Cols = cols;
        using value_type = int;

        value_type value_[Rows][Cols];
    } val_;

    param2d_wrapper(PdServ::ServBase &s, const char *path, unsigned int mode)
    {
        const size_t dims[2] = {rows, cols};
        s.parameter(path, mode, &val_.value_[0][0], 2, dims);
    };
};

using param03_type = param2d_wrapper<2, 3>;

class TestServer
{
    mutable std::mutex mut_;
    PdServ::Server server_;
    PdServ::Task task1_, task2_;
    Event1Type event1_;
    PdServ::Event single_event_;
    std::thread server_thread_;
    std::atomic_bool running_;
    param03_type param3_ {server_, "/dir1/param03", 0666};

  public:
    static constexpr auto task1_period = std::chrono::milliseconds {10};
    static constexpr auto task2_period = std::chrono::milliseconds {200};

    using signal2_type = std::array<short, 4>;
    using param2_type  = std::array<int, 4>;


    TestServer();
    ~TestServer();

    void start();
    void stop();

    void prepare();
    void set_config_file_path(const std::string &p);
    void clear();

    static constexpr clockid_t used_clock = CLOCK_MONOTONIC;
    static std::chrono::nanoseconds get_time();

    int tryPrepare();


    std::array<std::atomic_int, Event1Type::numberOfElements> event_control_;
    std::atomic_int single_event_control_;
    DECLARE_SIGNAL(double, signal1, task1_, 1, "/dir1/signal1")
    DECLARE_SIGNAL(double, signal12, task1_, 2, "/dir1/signal12")
    DECLARE_SIGNAL(double, signal11, task1_, 1, "/dir1/signal11")
    DECLARE_SIGNAL(signal2_type, signal2, task1_, 1, "/dir1/signal2")
    DECLARE_SIGNAL(int, signal5, task1_, 1, "/dir1/signal5")
    DECLARE_PARAMETER(int, param1, 0666, "/dir1/param01")
    DECLARE_PARAMETER(param2_type, param2, 0666, "/dir1/param02")
    DECLARE_SET_GET(param03_type::Value, param3);
    DECLARE_PARAMETER(int, param4, 0666, "/dir1/param04")
    DECLARE_PARAMETER(int, param5, 0666, "/dir1/param05")
    DECLARE_SIGNAL(
            unsigned int,
            time_counter_running,
            task1_,
            1,
            "/operation/hour/counter/running");
    DECLARE_PARAMETER(
            unsigned int,
            time_counter_restore,
            0666,
            "/operation/hour/counter/restore");
};


template <typename T>
constexpr timespec toTimespec(std::chrono::duration<int64_t, T> ms)
{
    const auto secs = std::chrono::duration_cast<std::chrono::seconds>(ms);
    return {secs.count(),
            std::chrono::duration_cast<std::chrono::nanoseconds>(ms - secs)
                    .count()};
}

template <typename T>
constexpr timeval toTimeval(std::chrono::duration<int64_t, T> ms)
{
    const auto secs = std::chrono::duration_cast<std::chrono::seconds>(ms);
    return {secs.count(),
            std::chrono::duration_cast<std::chrono::microseconds>(ms - secs)
                    .count()};
}
