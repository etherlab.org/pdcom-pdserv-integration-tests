/*****************************************************************************
 *
 * Copyright (C) 2021 Bjarne von Horn (vh at igh dot de).
 *
 * This file is part of the PdCom library.
 *
 * The PdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The PdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the PdCom library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

#ifndef PY_SSIZE_T_CLEAN
#define PY_SSIZE_T_CLEAN
#endif

#include "server.h"

#include <Python.h>
#include <pybind11/chrono.h>
#include <pybind11/numpy.h>
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>


namespace py = pybind11;

enum LogLevel {
    DUMMY,
};

template <typename T = param03_type::Value>
T createValue(py::tuple val)
{
    if (val.size() != T::Rows * T::Cols)
        throw std::invalid_argument("tuple must have six entries");

    T ans = {};
    for (unsigned int row = 0; row < T::Rows; ++row)
        for (unsigned int col = 0; col < T::Cols; ++col)
            ans.value_[row][col] =
                    py::cast<typename T::value_type>(val[row * T::Rows + col]);
    return ans;
}

template <typename T = param03_type::Value>
py::object getValue(const T &val)
{
    const Py_ssize_t dim[2] = {T::Rows, T::Cols};
    return py::array_t<typename T::value_type> {dim, &val.value_[0][0]};
}

PYBIND11_MODULE(MY_MODULE_NAME, m)
{
    py::class_<param03_type::Value>(m, "Param03Value")
            .def(py::init(&createValue<>))
            .def_property_readonly("value", &getValue<>);

    py::class_<LogLevel>(m, "Loglevel")
            .def_property_readonly_static(
                    "reset", [](py::object) { return RESET_EVENT; })
            .def_property_readonly_static(
                    "emergency", [](py::object) { return EMERG_EVENT; })
            .def_property_readonly_static(
                    "alert", [](py::object) { return ALERT_EVENT; })
            .def_property_readonly_static(
                    "error", [](py::object) { return ERROR_EVENT; })
            .def_property_readonly_static(
                    "warn", [](py::object) { return WARN_EVENT; })
            .def_property_readonly_static(
                    "notice", [](py::object) { return NOTICE_EVENT; })
            //
            ;
    py::class_<TestServer>(m, "TestServer")
            .def(py::init())
            .def_property(
                    "signal1", &TestServer::getsignal1, &TestServer::setsignal1)
            .def_property(
                    "signal2", &TestServer::getsignal2, &TestServer::setsignal2)
            .def_property(
                    "param2", &TestServer::getparam2, &TestServer::setparam2)
            .def_property(
                    "param3", &TestServer::getparam3, &TestServer::setparam3)
            .def("clear", &TestServer::clear)
            .def("start", &TestServer::start)
            .def("stop", &TestServer::stop)
            .def("prepare", &TestServer::prepare)
            .def("set_config_file_path", &TestServer::set_config_file_path)
            .def("set_event",
                 [](TestServer &s, int index, int level) {
                     s.event_control_.at(index) = level;
                 })
            .def("reset_event",
                 [](TestServer &s, int index) {
                     s.event_control_.at(index) = RESET_EVENT;
                 })
            .def("set_single_event",
                 [](TestServer &s, int level) {
                     s.single_event_control_ = level;
                 })
            .def("reset_single_event",
                 [](TestServer &s) { s.single_event_control_ = RESET_EVENT; })
            .def_property_readonly_static(
                    "task1_period",
                    [](py::object /* self */) {
                        return TestServer::task1_period;
                    })
            .def_property_readonly_static(
                    "task2_period",
                    [](py::object /* self */) {
                        return TestServer::task2_period;
                    })
            //
            ;
}
