/*****************************************************************************
 *
 * Copyright (C) 2021 Bjarne von Horn (vh at igh dot de)
 *
 * This file is part of the PdCom library.
 *
 * The PdCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The PdCom library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the PdCom library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

#include "server.h"

#include <time.h>


static void TsAdd(::timespec &ts1, const ::timespec &ts2)
{
    static constexpr int NS_IN_SEC = 1'000'000'000;
    // Add the two timespec variables
    ts1.tv_sec += ts2.tv_sec;
    ts1.tv_nsec += ts2.tv_nsec;
    // Check for nsec overflow
    if (ts1.tv_nsec >= NS_IN_SEC) {
        ts1.tv_sec++;
        ts1.tv_nsec -= NS_IN_SEC;
    }
}

std::chrono::nanoseconds TestServer::get_time()
{
    ::timespec ts;
    if (clock_gettime(used_clock, &ts))
        throw std::runtime_error("could not get clock");
    return std::chrono::seconds {ts.tv_sec}
    + std::chrono::nanoseconds {ts.tv_nsec};
}


TestServer::TestServer() :
    server_(
            "TestServer",
            "1.2.3",
            +[](timespec *time) { return clock_gettime(used_clock, time); }),
    task1_(server_.create_task(task1_period, "task1")),
    task2_(server_.create_task(task2_period, "task2")),
#ifdef OLD_PDSERV_API
    event1_(server_, WARN_EVENT, Event1Messages::path),
    single_event_(server_, Event2Path, ERROR_EVENT, Event2Text)
#else
    event1_(server_, Event1Messages::path),
    single_event_(server_, Event2Path, Event2Text)
#endif
{
    for (auto &i : event_control_)
        i = -1;
    single_event_control_ = -1;
#ifndef OLD_PDSERV_API
    task1_.set_signal_readlock(mut_);
    task2_.set_signal_readlock(mut_);
    server_.set_parameter_writelock(mut_);
#endif
}

void TestServer::start()
{
    stop();
    running_       = true;
    server_thread_ = std::thread {[this]() {
        constexpr clockid_t used_clock = CLOCK_MONOTONIC;
        unsigned int task_skip         = 0;
        constexpr auto max_task_skip   = TestServer::task2_period.count()
                / TestServer::task1_period.count();
        ::timespec actual_time;
        constexpr auto time_offset = toTimespec(task1_period);
        int counter                = 0;
        while (this->running_) {
            clock_gettime(used_clock, &actual_time);
#ifdef OLD_PDSERV_API
            pdserv_get_parameters(
                    server_.instance_.get(), task1_.task_, &actual_time);
#endif
            setsignal1(counter++);
            if (!(counter % 1))
                setsignal12(getsignal1());
            setsignal11(getsignal1() + 10);
            setsignal5(getparam5());
            for (unsigned int i = 0; i < Event1Type::numberOfElements; ++i) {
                if (event_control_[i] == RESET_EVENT) {
                    event1_.reset(i, actual_time);
                    event_control_[i] = -1;
                }
                else if (event_control_[i] > 0) {
#ifdef OLD_PDSERV_API
                    event1_.set(i, actual_time);
#else
                    event1_.set(i, event_control_[i], actual_time);
#endif
                    event_control_[i] = -1;
                }
            }
            if (single_event_control_ == RESET_EVENT) {
                single_event_.reset(actual_time);
                single_event_control_ = -1;
            }
            else if (single_event_control_ > 0) {
#ifdef OLD_PDSERV_API
                single_event_.set(actual_time);
#else
                single_event_.set(single_event_control_, actual_time);
#endif
                single_event_control_ = -1;
            }
            if (0 == task_skip) {
                task_skip = max_task_skip;
                this->task2_.update(actual_time);
            }
            task_skip--;
            this->task1_.update(actual_time);
            TsAdd(actual_time, time_offset);
            clock_nanosleep(used_clock, TIMER_ABSTIME, &actual_time, nullptr);
        }
    }};
}

void TestServer::stop()
{
    if (server_thread_.joinable()) {
        running_ = false;
        server_thread_.join();
    }
}

void TestServer::clear()
{
    stop();
    task1_.task_ = nullptr;
    task2_.task_ = nullptr;
    server_      = {};
}

TestServer::~TestServer()
{
    stop();
}

void TestServer::prepare()
{
    server_.prepare();
}

int TestServer::tryPrepare()
{
    return pdserv_prepare(server_.instance_.get());
}

void TestServer::set_config_file_path(const std::string &p)
{
    server_.set_config_file_path(p.c_str());
}

static_assert(
        TestServer::task1_period.count() < TestServer::task2_period.count()
                && TestServer::task2_period.count()
                                % TestServer::task1_period.count()
                        == 0,
        "Bigger period must be a multiple of the smaller one");

const std::chrono::milliseconds TestServer::task1_period;
const std::chrono::milliseconds TestServer::task2_period;
