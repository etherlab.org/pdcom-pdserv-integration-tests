/*****************************************************************************
 *
 *  Copyright 2021 Bjarne von Horn (vh at igh dot de)
 *
 *  This file is part of the pdserv library.
 *
 *  The pdserv library is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or (at
 *  your option) any later version.
 *
 *  The pdserv library is distributed in the hope that it will be useful, but
 *  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 *  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 *  License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with the pdserv library. If not, see <http://www.gnu.org/licenses/>.
 *
 *****************************************************************************/

#pragma once

#include <array>
#include <chrono>
#include <cstddef>
#include <cstdint>
#include <memory>
#include <mutex>
#include <pdserv.h>
#include <stdexcept>
#include <type_traits>

#ifdef OLD_PDSERV_API
#define RESET_EVENT 0
typedef int (*gettime_t)(struct timespec *);
#endif

namespace PdServ {
namespace details {

template <typename T>
struct DatatypeTraits
{};

template <>
struct DatatypeTraits<double>
{
    static constexpr int identifier = pd_double_T;
};
template <>
struct DatatypeTraits<float>
{
    static constexpr int identifier = pd_single_T;
};
template <>
struct DatatypeTraits<uint8_t>
{
    static constexpr int identifier = pd_uint8_T;
};
template <>
struct DatatypeTraits<int8_t>
{
    static constexpr int identifier = pd_sint8_T;
};
template <>
struct DatatypeTraits<uint16_t>
{
    static constexpr int identifier = pd_uint16_T;
};
template <>
struct DatatypeTraits<int16_t>
{
    static constexpr int identifier = pd_sint16_T;
};
template <>
struct DatatypeTraits<uint32_t>
{
    static constexpr int identifier = pd_uint32_T;
};
template <>
struct DatatypeTraits<int32_t>
{
    static constexpr int identifier = pd_sint32_T;
};
template <>
struct DatatypeTraits<uint64_t>
{
    static constexpr int identifier = pd_uint64_T;
};
template <>
struct DatatypeTraits<int64_t>
{
    static constexpr int identifier = pd_sint64_T;
};
template <>
struct DatatypeTraits<bool>
{
    static constexpr int identifier = pd_boolean_T;
};

#ifndef OLD_PDSERV_API
template <>
struct DatatypeTraits<char>
{
    static constexpr int identifier = pd_char_T;
};
#endif

template <typename T>
constexpr bool int_or_float()
{
    return std::is_integral<T>::value || std::is_floating_point<T>::value;
}
}  // namespace details

struct Variable
{
    struct create_failed : public std::runtime_error
    {
        create_failed() :
            std::runtime_error("Could not create pdvariable instance")
        {}
    };

    ::pdvariable *variable_;

    Variable(::pdvariable *variable) : variable_(variable)
    {
        if (!variable)
            throw create_failed();
    }

    void set_alias(const char *alias) { pdserv_set_alias(variable_, alias); }

    void set_unit(const char *unit) { pdserv_set_unit(variable_, unit); }

    void set_comment(const char *comment)
    {
        pdserv_set_comment(variable_, comment);
    }
};

struct Task
{
    ::pdtask *task_;

    struct create_failed : public std::runtime_error
    {
        create_failed() : std::runtime_error("Could not create pdtask instance")
        {}
    };

    Task(::pdtask *task) : task_(task)
    {
        if (!task)
            throw create_failed();
    }

    template <typename T>
    Variable
    signal(unsigned int decimation,
           const char *path,
           T const *addr,
           size_t n,
           const size_t *dim = nullptr)
    {
        constexpr auto dtype = details::DatatypeTraits<T>::identifier;
        return {pdserv_signal(task_, decimation, path, dtype, addr, n, dim)};
    }

    template <typename T>
    typename std::enable_if<std::is_arithmetic<T>::value, Variable>::type
    signal(unsigned int decimation, const char *path, T const &var)
    {
        constexpr auto dtype = details::DatatypeTraits<T>::identifier;
        return {pdserv_signal(
                task_, decimation, path, dtype, &var, 1, nullptr)};
    }
    template <typename T>
    typename std::enable_if<!std::is_arithmetic<T>::value, Variable>::type
    signal(unsigned int decimation, const char *path, T const &var)
    {
        constexpr auto dtype =
                details::DatatypeTraits<typename T::value_type>::identifier;
        return {pdserv_signal(
                task_, decimation, path, dtype, var.data(), var.size(),
                nullptr)};
    }
    template <typename T>
    Variable
    signal(unsigned int decimation, const char *path, T const &&var) = delete;

    void update(const timespec &t = {0, 0}) { pdserv_update(task_, &t); }

#ifndef OLD_PDSERV_API
    template <class T, void (T::*cb)(int)>
    void set_signal_writelock_cb(T *t)
    {
        pdserv_set_signal_readlock_cb(
                task_,
                +[](int lock, void *p) {
                    T &obj = *reinterpret_cast<T *>(p);
                    (obj.*cb)(lock);
                },
                t);
    }

    void set_signal_readlock(std::mutex &mut)
    {
        pdserv_set_signal_readlock_cb(
                task_,
                [](int lock, void *m) {
                    std::mutex &mut(*reinterpret_cast<std::mutex *>(m));
                    if (lock)
                        mut.lock();
                    else
                        mut.unlock();
                },
                &mut);
    }
#endif

    void
    update_statistics(double exc_time, double cycle_time, unsigned int overrun)
    {
        pdserv_update_statistics(task_, exc_time, cycle_time, overrun);
    }
};

class ServBase
{
  public:
    struct ServDeleter
    {
        void operator()(::pdserv *serv) { pdserv_exit(serv); }
    };
    std::unique_ptr<::pdserv, ServDeleter> instance_;

    struct create_failed : public std::runtime_error
    {
        create_failed() : std::runtime_error("Could not create pdserv instance")
        {}
    };

    struct prepare_failed : public std::runtime_error
    {
        prepare_failed() : std::runtime_error("Could not prepare pdserv") {}
    };

    ServBase() = default;
    ServBase(const char *name, const char *version, ::gettime_t gettime_cb) :
        instance_(pdserv_create(name, version, gettime_cb))
    {
        if (!instance_)
            throw create_failed();
    }
    ServBase(ServBase &&) = default;
    ServBase &operator=(ServBase &&) = default;

    void set_config_file_path(const char *file)
    {
        pdserv_config_file(instance_.get(), file);
    }

    void prepare()
    {
        if (pdserv_prepare(instance_.get()) != 0)
            throw prepare_failed();
    }

    Task create_task(std::chrono::duration<double> tsample, const char *name)
    {
        auto ans = pdserv_create_task(instance_.get(), tsample.count(), name);
        return {ans};
    }

#ifndef OLD_PDSERV_API
    void set_parameter_writelock(std::mutex &mut)
    {
        pdserv_set_parameter_writelock_cb(
                instance_.get(),
                [](int lock, void *m) {
                    std::mutex &mut(*reinterpret_cast<std::mutex *>(m));
                    if (lock)
                        mut.lock();
                    else
                        mut.unlock();
                },
                &mut);
    }

    template <typename T, void (T::*fn)(int)>
    void set_parameter_writelock_cb(T *t)
    {
        pdserv_set_parameter_writelock_cb(
                instance_.get(),
                [](int lock, void *arg) {
                    (reinterpret_cast<T *>(arg)->*fn)(lock);
                },
                t);
    }

#endif

    template <
            typename C,
            int (C::*cb)(
                    const ::pdvariable *,
                    void *,
                    const void *,
                    size_t,
                    ::timespec *),
            typename T>
    Variable parameter(
            C *c,
            const char *path,
            unsigned int mode,
            T *addr,
            size_t n,
            const size_t *dim = nullptr)
    {
        constexpr auto dtype  = details::DatatypeTraits<T>::identifier;
        pdvariable *const ans = pdserv_parameter(
                instance_.get(), path, mode, dtype, addr, n, dim,
                [](const struct pdvariable *param, void *dst, const void *src,
                   size_t len, struct timespec *time, void *priv_data) {
                    auto &obj = *reinterpret_cast<C *>(priv_data);
                    (obj.*cb)(param, dst, src, len, time);
                },
                c);
        return {ans};
    }

    template <
            typename C,
            int (C::*cb)(
                    const ::pdvariable *,
                    void *,
                    const void *,
                    size_t,
                    ::timespec *),
            typename T>
    typename std::enable_if<details::int_or_float<T>(), Variable>::type
    parameter(C *c, const char *path, unsigned int mode, T &val)
    {
        constexpr auto dtype  = details::DatatypeTraits<T>::identifier;
        pdvariable *const ans = pdserv_parameter(
                instance_.get(), path, mode, dtype, &val, 1, nullptr,
                [](const struct pdvariable *param, void *dst, const void *src,
                   size_t len, struct timespec *time, void *priv_data) {
                    auto &obj = *reinterpret_cast<C *>(priv_data);
                    (obj.*cb)(param, dst, src, len, time);
                },
                c);
        return {ans};
    }

    template <
            typename C,
            int (C::*cb)(
                    const ::pdvariable *,
                    void *,
                    const void *,
                    size_t,
                    ::timespec *),
            typename T>
    typename std::enable_if<!details::int_or_float<T>(), Variable>::type
    parameter(C *c, const char *path, unsigned int mode, T &val)
    {
        constexpr auto dtype =
                details::DatatypeTraits<typename T::value_type>::identifier;
        pdvariable *const ans = pdserv_parameter(
                instance_.get(), path, mode, dtype, val.data(), val.size(),
                nullptr,
                [](const struct pdvariable *param, void *dst, const void *src,
                   size_t len, struct timespec *time, void *priv_data) {
                    auto &obj = *reinterpret_cast<C *>(priv_data);
                    (obj.*cb)(param, dst, src, len, time);
                },
                c);
        return {ans};
    }

    template <typename T>
    Variable parameter(
            const char *path,
            unsigned int mode,
            T *addr,
            size_t n,
            const size_t *dim = nullptr)
    {
        constexpr auto dtype = details::DatatypeTraits<T>::identifier;
        return {pdserv_parameter(
                instance_.get(), path, mode, dtype, addr, n, dim, nullptr,
                nullptr)};
    }
    template <typename T, size_t N, size_t M>
    Variable
    parameter(const char *path, unsigned int mode, const T (&val)[N][M])
    {
        constexpr auto dtype = details::DatatypeTraits<T>::identifier;
        const size_t dim[]   = {N, M};
        return {pdserv_parameter(
                instance_.get(), path, mode, dtype, &val, 2, &dim, nullptr,
                nullptr)};
    }
    template <typename T>
    typename std::enable_if<details::int_or_float<T>(), Variable>::type
    parameter(const char *path, unsigned int mode, T &val)
    {
        constexpr auto dtype = details::DatatypeTraits<T>::identifier;
        return {pdserv_parameter(
                instance_.get(), path, mode, dtype, &val, 1, nullptr, nullptr,
                nullptr)};
    }
    template <typename T>
    typename std::enable_if<!details::int_or_float<T>(), Variable>::type
    parameter(const char *path, unsigned int mode, T &val)
    {
        constexpr auto dtype =
                details::DatatypeTraits<typename T::value_type>::identifier;
        return {pdserv_parameter(
                instance_.get(), path, mode, dtype, val.data(), val.size(),
                nullptr, nullptr, nullptr)};
    }

  protected:
    ~ServBase() = default;
};

struct Server : public ServBase
{
    using ServBase::ServBase;
};

class Compound
{
    int id_;

  public:
    Compound(const char *name, size_t size) :
        id_(pdserv_create_compound(name, size))
    {}

    int get_id() const { return id_; }

    void add_field(
            const char *name,
            int data_type,
            size_t offset,
            size_t ndims,
            const size_t *dim = nullptr)
    {
        pdserv_compound_add_field(id_, name, data_type, offset, ndims, dim);
    }
};

namespace detail {
template <size_t N>
constexpr size_t numberOfCharP(const char *const (&)[N])
{
    return N;
}
template <size_t N>
constexpr size_t numberOfCharP(std::array<const char *, N> const &)
{
    return N;
}
}  // namespace detail

class Event
{
    std::array<const char *, 1> charp_;

  public:
#ifdef OLD_PDSERV_API
    const pdevent *event_;

    Event(const ServBase &serv,
          const char *path,
          int priority,
          const char *text = nullptr) :
        charp_({text}),
        event_(pdserv_event(
                serv.instance_.get(),
                path,
                priority,
                1,
                text ? charp_.data() : nullptr))
    {
        if (!event_) {
            throw std::runtime_error("could not create Event");
        }
    }


    void set(const timespec &ts) { pdserv_event_set(event_, 0, 1, &ts); }

    void reset(const timespec &ts) { pdserv_event_set(event_, 0, 0, &ts); }

#else
    pdevent *event_;

    Event(const ServBase &serv, const char *path, const char *text) :
        charp_({text}), event_(pdserv_event(serv.instance_.get(), path, 1))
    {
        if (!event_) {
            throw std::runtime_error("could not create Event");
        }
        if (text) {
            pdserv_event_set_text(event_, charp_.data());
        }
    }

    Event(Event &&o) noexcept : charp_({o.charp_[0]}), event_(o.event_)
    {
        // adjust char pointer
        if (event_ && charp_[0])
            pdserv_event_set_text(event_, charp_.data());
    }

    Event &operator=(Event &&other) noexcept
    {
        charp_[0] = other.charp_[0];
        event_    = other.event_;
        if (event_ && charp_[0])
            pdserv_event_set_text(event_, charp_.data());
        return *this;
    }

    void set(int priority, const timespec &ts)
    {
        pdserv_event_set(event_, 0, priority, &ts);
    }

    void reset(const timespec &ts) { pdserv_event_reset(event_, 0, &ts); }
#endif
};

template <class StringContainer>
struct MultiDimensionEvent
{
    static constexpr size_t numberOfElements =
            detail::numberOfCharP(StringContainer::messages);

#ifdef OLD_PDSERV_API
    std::array<const char *, numberOfElements> texts_;
    const pdevent *event_;

    static std::array<const char *, numberOfElements> fillArray()
    {
        std::array<const char *, numberOfElements> ans;
        std::copy(
                std::begin(StringContainer::messages),
                std::end(StringContainer::messages), ans.begin());
        return ans;
    }


    MultiDimensionEvent(const ServBase &serv, int priority, const char *path) :
        texts_(fillArray()),
        event_(pdserv_event(
                serv.instance_.get(),
                path,
                priority,
                numberOfElements,
                texts_.data()))
    {
        if (!event_)
            throw std::runtime_error("could not create Event");
    }

    void set(size_t element, const timespec &ts)
    {
        pdserv_event_set(event_, element, 1, &ts);
    }

    void reset(size_t element, const timespec &ts)
    {
        pdserv_event_set(event_, element, 0, &ts);
    }


    void set_all(const timespec &ts)
    {
        for (size_t i = 0; i < numberOfElements; ++i)
            set(i, ts);
    }

    void reset_all(const timespec &ts)
    {
        for (size_t i = 0; i < numberOfElements; ++i)
            reset(i, ts);
    }
#else
    pdevent *event_;

    MultiDimensionEvent(const ServBase &serv, const char *path) :
        event_(pdserv_event(serv.instance_.get(), path, numberOfElements))
    {
        if (!event_)
            throw std::runtime_error("could not create Event");
        pdserv_event_set_text(event_, &StringContainer::messages[0]);
    }

    void set(size_t element, int priority, const timespec &ts)
    {
        pdserv_event_set(event_, element, priority, &ts);
    }

    void reset(size_t element, const timespec &ts)
    {
        pdserv_event_reset(event_, element, &ts);
    }

    void
    set_all(const unsigned int (&level)[numberOfElements], const timespec &ts)
    {
        pdserv_event_set_all(event_, &level, &ts);
    }

    void
    set_all(const std::array<unsigned int, numberOfElements> &level,
            const timespec &ts)
    {
        pdserv_event_set_all(event_, level.data(), &ts);
    }

#endif
};
}  // namespace PdServ
