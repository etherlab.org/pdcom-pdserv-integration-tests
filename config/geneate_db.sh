#!/bin/sh

export PATH=$PATH:/usr/sbin
echo "Bob!" | saslpasswd2 -p -u example.com -c -f $1 bob
echo "Alice!" | saslpasswd2 -p -u example.com -c -f $1 alice
echo "1nSecure" | saslpasswd2 -p -u test.example.com -c -f $1 ernie
