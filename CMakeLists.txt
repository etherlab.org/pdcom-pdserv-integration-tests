cmake_minimum_required(VERSION 3.10)

project(PdComIntegrationTests)

set(PDCOM_MAJOR_VERSIONS 3;5 CACHE STRING "List of PdCom Major Versions to test")
set(NOSE2_LD_PRELOAD "" CACHE STRING "LD_PRELOAD for nose2 plugin (to help with asan)")

find_package(GTest REQUIRED)
find_package(PdServ REQUIRED)
include(CTest)
enable_testing()
add_subdirectory(certs)
set(CERT_DIR ${CMAKE_CURRENT_BINARY_DIR}/certs)
set(CONF_DIR ${CMAKE_CURRENT_SOURCE_DIR}/config)

if (3 IN_LIST PDCOM_MAJOR_VERSIONS)
    add_subdirectory(src/pdcom3)
endif()

if (5 IN_LIST PDCOM_MAJOR_VERSIONS)
    add_subdirectory(src/pdcom5)
endif()

get_target_property(PDSERV_LOCATION EtherLab::pdserv LOCATION)
get_filename_component(PDSERV_LIBDIR "${PDSERV_LOCATION}" DIRECTORY)
get_target_property(PDSERV_INCLUDE_DIR EtherLab::pdserv INTERFACE_INCLUDE_DIRECTORIES)

find_package(PkgConfig)
pkg_check_modules(PDSERV110 REQUIRED "libpdserv=1.1.0")

configure_file(src/setup.py.in ${CMAKE_CURRENT_SOURCE_DIR}/src/setup.py)
configure_file(config/pdserv.tls-simple.conf.in ${CMAKE_CURRENT_SOURCE_DIR}/config/pdserv.tls-simple.conf)
configure_file(config/pdserv.tls-simple-altport.conf.in ${CMAKE_CURRENT_SOURCE_DIR}/config/pdserv.tls-simple-altport.conf)
configure_file(config/pdserv.tls-clientauth.conf.in ${CMAKE_CURRENT_SOURCE_DIR}/config/pdserv.tls-clientauth.conf)
configure_file(config/pdserv.sasl.conf.in ${CMAKE_CURRENT_SOURCE_DIR}/config/pdserv.sasl.conf)
configure_file(config/pdserv.sasl_voluntary.conf.in ${CMAKE_CURRENT_SOURCE_DIR}/config/pdserv.sasl_voluntary.conf)
configure_file(config/pdserv.persistent.conf.in ${CMAKE_CURRENT_SOURCE_DIR}/config/pdserv.persistent.conf)
configure_file(config/pdserv.persistent-pair.conf.in ${CMAKE_CURRENT_SOURCE_DIR}/config/pdserv.persistent-pair.conf)
add_custom_command(OUTPUT passwd.db COMMAND ${CMAKE_CURRENT_SOURCE_DIR}/config/geneate_db.sh ARGS passwd.db)
add_custom_target(PasswdDb ALL DEPENDS passwd.db)
