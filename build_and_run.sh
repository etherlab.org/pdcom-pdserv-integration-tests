#!/bin/sh

set -e

: "${PDCOM_MAJOR_VERSIONS:=3;5}" "${WORKSPACE:=/tmp/pdcom_integration_tests}"
: "${PDCOM3_BRANCH:=stable-3.0}" "${PDCOM5_BRANCH:=master}"
: "${PDCOM_REPO:=https://gitlab.com/etherlab.org/pdcom.git}"
: "${PDSERV_REPO:=https://gitlab.com/etherlab.org/pdserv.git}" "${PDSERV_BRANCH:=master}"
: "${QTPDCOM_REPO:=https://gitlab.com/etherlab.org/qtpdcom.git}" "${QTPDCOM_BRANCH:=main}"
: "${LIBDIR:=lib64}"

if [ -e "${WORKSPACE}" ]; then
    echo "Workspace exists, stopping" >&2
    exit 1;
fi;

echo -e "\e[0Ksection_start:`date +%s`:pdserv\r\e[0KInstalling PdServ (Branch ${PDSERV_BRANCH})"
mkdir -p "${WORKSPACE}/install" "${WORKSPACE}/src"
pushd "${WORKSPACE}/src"

git clone --depth=1 -b ${PDSERV_BRANCH} $PDSERV_REPO pdserv
mkdir pdserv/build
pushd pdserv/build
echo "checked out PdServ Revision $(git rev-parse --short HEAD)"
cmake -DCMAKE_BUILD_TYPE=Debug -DCMAKE_INSTALL_PREFIX="${WORKSPACE}/install" -DCMAKE_CXX_FLAGS="$CXXFLAGS" -DCMAKE_SHARED_LINKER_FLAGS="$LDFLAGS" -DCMAKE_EXE_LINKER_FLAGS="$LDFLAGS" -DENABLE_DOC=0 ..
make -j4 install
popd

git clone --depth=1 -b fixed_release-1.1.0 https://gitlab.com/etherlab.org/pdserv.git pdserv-1.1.0
mkdir pdserv-1.1.0/build
pushd pdserv-1.1.0/build
echo "checked out PdServ Revision $(git rev-parse --short HEAD)"
cmake -DCMAKE_BUILD_TYPE=Debug -DCMAKE_INSTALL_PREFIX="${WORKSPACE}/install/pdserv-1.1.0" -DCMAKE_CXX_FLAGS="$CXXFLAGS" -DCMAKE_SHARED_LINKER_FLAGS="$LDFLAGS" -DCMAKE_EXE_LINKER_FLAGS="$LDFLAGS" ..
make -j4 install
popd

echo -e "\e[0Ksection_end:`date +%s`:pdserv\r\e[0K"

for VERSION in ${PDCOM_MAJOR_VERSIONS//;/ }; do
    BRANCH_VAR="PDCOM${VERSION}_BRANCH"
    echo -e "\e[0Ksection_start:`date +%s`:pdcom${VERSION}\r\e[0KInstalling PdCom${VERSION} (Branch ${!BRANCH_VAR})"
    git clone --depth=1 -b "${!BRANCH_VAR}" $PDCOM_REPO pdcom${VERSION}
    mkdir "pdcom${VERSION}/build"
    pushd "pdcom${VERSION}/build"
    echo "checked out PdCom${VERSION} Revision $(git rev-parse --short HEAD)"
    cmake \
        -DCMAKE_BUILD_TYPE=Debug \
        -DCMAKE_INSTALL_PREFIX="${WORKSPACE}/install" \
        -DCMAKE_CXX_FLAGS="$CXXFLAGS" \
        -DCMAKE_SHARED_LINKER_FLAGS="$LDFLAGS" \
        -DCMAKE_EXE_LINKER_FLAGS="$LDFLAGS" \
        -DCMAKE_DISABLE_FIND_PACKAGE_Doxygen=ON \
        ..
    make -j4 install
    ctest --verbose
    if [ ${VERSION} -eq 5 ]; then
        cd ../python
        python3 -m pip install .
    fi
    popd
    echo -e "\e[0Ksection_end:`date +%s`:pdcom${VERSION}\r\e[0K"
done

echo -e "\e[0Ksection_start:`date +%s`:qtpdcom\r\e[0KInstalling QtPdCom1 (Branch ${QTPDCOM_BRANCH})"

git clone --depth=1 -b ${QTPDCOM_BRANCH} $QTPDCOM_REPO qtpdcom
mkdir qtpdcom/build && pushd qtpdcom/build
echo "checked out QtPdCom Revision $(git rev-parse --short HEAD)"
cmake -DCMAKE_BUILD_TYPE=Debug -DCMAKE_INSTALL_PREFIX="${WORKSPACE}/install" \
    -DCMAKE_CXX_FLAGS="$CXXFLAGS" -DCMAKE_SHARED_LINKER_FLAGS="$LDFLAGS" \
    -DBUILD_TESTING=1 \
    -DCMAKE_PREFIX_PATH="${WORKSPACE}/install" -DCMAKE_EXE_LINKER_FLAGS="$LDFLAGS" ..
make -j4 install
ctest --verbose
echo -e "\e[0Ksection_end:`date +%s`:qtpdcom\r\e[0K"
popd

popd

echo -e "\e[0Ksection_start:`date +%s`:build_tests\r\e[0KInstalling Test Cases"
mkdir -p build
pushd build
PKG_CONFIG_PATH="${WORKSPACE}/install/pdserv-1.1.0/${LIBDIR}/pkgconfig" \
cmake -DCMAKE_BUILD_TYPE=Debug \
    -DCMAKE_PREFIX_PATH=${WORKSPACE}/install \
    -DPDCOM_MAJOR_VERSIONS=${PDCOM_MAJOR_VERSIONS} \
    -DCMAKE_CXX_FLAGS="$CXXFLAGS" \
    -DCMAKE_SHARED_LINKER_FLAGS="$LDFLAGS" \
    -DCMAKE_EXE_LINKER_FLAGS="$LDFLAGS" \
    -DNOSE2_LD_PRELOAD="$NOSE2_LD_PRELOAD" ..
pushd ../src
python3 -m pip install .
popd
make -j4
echo -e "\e[0Ksection_end:`date +%s`:build_tests\r\e[0K"

echo -e "\e[0Ksection_start:`date +%s`:run_tests\r\e[0KRunning Test Cases"
export LD_LIBRARY_PATH=${WORKSPACE}/install/$LIBDIR
ctest --verbose
popd
echo -e "\e[0Ksection_end:`date +%s`:run_tests\r\e[0K"
