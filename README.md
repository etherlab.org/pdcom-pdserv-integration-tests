
[![coverage report](https://gitlab.com/etherlab.org/pdcom-pdserv-integration-tests/badges/main/coverage.svg)](https://docs.etherlab.org/pdcom-pdserv-integration-tests/1/coverage/index.html)
[![pipeline status](https://gitlab.com/etherlab.org/pdcom-pdserv-integration-tests/badges/main/pipeline.svg)](https://gitlab.com/etherlab.org/pdcom-pdserv-integration-tests/-/commits/main)

PdCom and PdServ integration tests
==================================

This repository will provide tools to test different versions of [PdCom](https://gitlab.com/etherlab.org/pdcom) and [PdServ](https://gitlab.com/etherlab.org/pdserv) against each other.
